.include "nes.inc"
.include "global.inc"

.segment "BSS"

AI_MODE_NORMAL = 0
AI_MODE_STUPID = 1

ai_keys:    .res 4
ai_player:  .res 1
ai_mode:    .res 4
ai_timer:   .res 4
ai_destx:   .res 4
ai_desty:   .res 4

.segment "CODE"

.proc runAi
tmp = 0

  ;alternate players to check ai
  lda timer
  and #%00000011
  sta tmp

  cpx tmp
  beq runPlayerAi

  ;if we don't run, use the last keys
  lda ai_keys,x
  sta cur_keys,x

  rts



runPlayerAi:

  stx ai_player

  ;if timer is 0, pick a new mode
  lda ai_timer,x
  bne :+
    jsr pickNewMode
:

  ldx ai_player
  lda player_status,x
  cmp #PLAYER_STATUS_RESPAWNING
  bne :+
    jsr pickNewMode
:



  dec ai_timer,x

  ;move toward dest
  
  ldx ai_player

  ;debug
  lda ai_destx,x
  sta debugData,x

  ;horiz
  lda player_xhi,x
  sbc ai_destx,x
  bcc right
    absoluteValue
    cmp #20
    bcc vert
    lda #KEY_LEFT
    sta cur_keys,x
    jmp vert
right:
    absoluteValue
    cmp #20
    bcc vert
    lda #KEY_RIGHT
    sta cur_keys,x

   ;vertical
vert:
  lda player_yhi,x
  sbc ai_desty,x
  bcs up
    absoluteValue
    cmp #20
    bcc done
    lda #KEY_DOWN
    ora cur_keys,x
    sta cur_keys,x
    jmp done
up:
    absoluteValue
    cmp #20
    bcc done
    lda #KEY_UP
    ora cur_keys,x
    sta cur_keys,x

done:

    ;see if we shoot
    jsr randomNumber
    ldx ai_player
    and #%00011111
    bne :+
      lda cur_keys,x
      ora #KEY_A
      sta cur_keys,x
:

    lda cur_keys,x
    sta ai_keys,x

  rts
.endproc

.proc pickNewMode
  jsr randomNumber
  and #$0F
  ldx ai_player
  sta ai_timer,x
  and #%00000111
  sta ai_mode,x
  bne moveTowardGap
  jmp doSomethingStupid
.endproc


.proc moveTowardGap
tmpa = 4
tmpb = 5
  ;find the next barrier
  ldx ai_player
  lda player_yhi,x
  sta tmpa
:
  lda tmpa
  sec
  sbc #16
  bcc giveUp
  sta tmpa
  jsr getBarrierAtA
  cmp #BARRIER_NONE
  beq :-

  ;find the center of gap
  tay
  lda bg_barrier_opening_size,y
  clc
  ror
  adc bg_barrier_opening_offset,y
  clc
  asl
  asl
  asl
  sta tmpa

  ;pick a random modifier
  jsr randomNumber
  and #%00011111
  sbc #16
  sta tmpb

  ldx ai_player

  lda tmpa
  adc tmpb
  sta ai_destx,x


  lda player_yhi,x
  sta ai_desty,x
  ;go up if we're close enough
  lda ai_destx,x
  sbc player_xhi,x
  absoluteValue
  cmp #90
  bcs :+

    lda player_yhi,x
    sbc #60
    sta ai_desty,x
:

  rts
giveUp:
  ldx ai_player
  lda player_yhi,x
  sbc #16
  sta ai_desty,x
  lda player_xhi,x
  sta ai_destx,x
  rts

.endproc

.proc doSomethingStupid
  jsr randomNumber
  ldx ai_player
  sta ai_destx,x
  jsr randomNumber
  ldx ai_player
  sta ai_destx,x
  rts

.endproc
