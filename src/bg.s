.include "nes.inc"
.include "global.inc"

BLANK_TILE = 4
BOX_TILE = $82
STAR_TILE = $40
BOX_ATTR_PAL = 1
BLANK_ATTR_PAL = 0

BARRIER_DEAD = 241


NUM_BARRIERS = 4

.segment "ZEROPAGE"
shouldDrawNewBg: .res 1

.segment "BSS"


bg_last_drawn_lo:             .res 1

bg_barrier_opening_offset:   .res NUM_BARRIERS
bg_barrier_opening_size:     .res NUM_BARRIERS
bg_barrier_y:                .res NUM_BARRIERS
bg_barrier_scored:           .res NUM_BARRIERS


bg_new_barrier_in:    .res 1
bg_new_barrier_in_lo:    .res 1

bg_scroll_lo:         .res 1
bg_scroll_hi:         .res 1
bg_scroll_speed_lo:   .res 1
bg_scroll_speed_hi:   .res 1
bg_top_at:            .res 1


bg_draw_addr_lo:      .res 1
bg_draw_addr_hi:      .res 1
bg_draw_tile:         .res 64

bg_draw_attr_hi:      .res 1
bg_draw_attr_lo:      .res 1
bg_draw_attr:         .res 16

attr_shadow:          .res 64

bg_tmp:               .res 1

.segment "CODE"

.proc initBackground

  ;clear the barriers
  lda #BARRIER_DEAD
  ldx #NUM_BARRIERS-1
:
    sta bg_barrier_y,x
  dex
  bpl :-

  ;draw score headers

  ;reset bg scroll
  lda #0
  sta bg_scroll_lo
  sta bg_scroll_hi

  jmp drawScoreHeaders
.endproc

.proc midFrameScroll
temp = 0


  ; change the scrolling
  ; step 1, write nametable << 2 to PPUADDR
  lda #0
  clc
  asl
  asl
  sta PPUADDR
  ; step 2  - y to PPUSCROLL
  lda mid_scroll_y
  sta PPUSCROLL
  ; step 3  - x to PPUSCROLL
  lda #0
  sta PPUSCROLL
  ; step 4 -- evil voodoo, see http://wiki.nesdev.com/w/index.php/PPU_scrolling#Split_X.2FY_scroll
  lda mid_scroll_voodoo
  sta PPUADDR

  rts

.endproc



.proc updateBackground

oldScroll = 0
scrolledBy = 1

  lda bg_scroll_hi
  sta oldScroll

  ;update scroll position
  subTwo16s bg_scroll_speed_lo,bg_scroll_speed_hi,bg_scroll_lo,bg_scroll_hi
  php ;save carry
  pha

  ;update respawn-in
  lda bg_new_barrier_in
  beq afterNewBarrierUpdate
  subTwo16s bg_scroll_speed_lo,bg_scroll_speed_hi,bg_new_barrier_in_lo,bg_new_barrier_in
  bcs afterNewBarrierUpdate
  lda #0
  sta bg_new_barrier_in

afterNewBarrierUpdate:


  ;figure out how far we actually scrolled
  lda oldScroll
  sec
  sbc bg_scroll_hi
  sta scrolledBy

  ;advance all the acgive barriers by amount scrolled
  ldx #NUM_BARRIERS-1
:
  lda bg_barrier_y,x
  cmp #BARRIER_DEAD
  beq skipUpdateBarrier
  clc
  adc scrolledBy
  ;if it's more than 245, we probably just spawned it
  cmp #245
  bcs storeBarrierY
  ;if its more than 240 (and less than 245), then it's dead
  cmp #240
  bcc storeBarrierY
  lda #BARRIER_DEAD
storeBarrierY:
  sta bg_barrier_y,x

  ;if more than 240, don't score it
  cmp #240
  bcs skipUpdateBarrier

  ;check for scoring the barrier
  lda bg_barrier_scored,x
  bne skipUpdateBarrier
  jsr checkForScoringBarrier
  
skipUpdateBarrier:

  dex
  bpl :-


  pla
  plp ;grab carry to see if we need to reset the scroll
  bcs :+
  adc #239
  sta bg_scroll_hi
:


  ;see if we need to generate a new barrier
  jsr maybeSpawnBarrier
  rts
.endproc

.proc checkForScoringBarrier
  lda bg_barrier_y,x
  sta bg_tmp
  ldy #3
:
  ;see if player in game
  lda player_ingame,y
  beq noScore


  lda bg_tmp

  ;if so, compare their position
  cmp player_yhi,y
  bcc noScore

  ;only score if alive and well
  lda player_status,y
  cmp #PLAYER_STATUS_NORMAL
  bne noScore

  ;score barrier
  lda #1
  sta bg_barrier_scored,x
  lda player_score,y
  clc
  adc #1
  sta player_score,y


noScore:
  dey
  bpl :-
  rts

.endproc

.proc maybeSpawnBarrier
  lda bg_new_barrier_in
  beq :+
  rts
:

  ;find one to spawn
  ldx #NUM_BARRIERS - 1
:
  lda bg_barrier_y,x
  eor #BARRIER_DEAD
  beq spawnBarrier
  dex
  bpl :-
  ;couldn't find one, don't spawn it
  rts

spawnBarrier:
  lda #0
  sta bg_barrier_scored,x
  jmp generateBarrier ;use the rts from generateBarrier
.endproc


; a is point to check, y is size, x is barrier #
.proc checkPointAgainstBarrier

offset    = 2
size      = 3
gapright  = 4
point     = 5

  sta point

  ;calc pixel-level dimensions of opening
  lda bg_barrier_opening_size,x
  asl ;x2
  asl ;x4
  asl ;x8
  sta size

  lda bg_barrier_opening_offset,x
  clc
  adc #1
  asl ;x2
  asl ;x4
  asl ;x8
  sta offset

  clc
  adc size
  sbc #8
  sta gapright

  ;calc right side
  tya
  clc
  adc point

  ;if right side less than opening offset, collision
  cmp offset
  bcc collision

  ;if left side greater than opening offset+size, collision
  lda point
  cmp gapright
  bcs collision

  lda #0
  rts

collision:
  lda #1
  rts

.endproc


.proc getBarrierAtA
temp = 2

  sta temp
  ldx #NUM_BARRIERS - 1
:
  lda bg_barrier_y,x
  sec
  sbc temp
  bcs notThisOne
  adc #16
  beq notThisOne
  bcc notThisOne
  ;this one
  txa
  rts

notThisOne:
  dex
  bpl :-

  lda #BARRIER_NONE
  rts

.endproc

.proc preRenderBackground

    ;for now, always re-draw the top line. maybe change that later
    ;to not waste cycles
    jsr calcDrawAddress

 
    ;first see if we need to draw a barrier
    lda #17
    jsr getBarrierAtA
    cmp #BARRIER_NONE
    bne renderThisBarrier


    ;if not, see if we've already drawn this render line
    lda bg_draw_addr_lo
    eor bg_draw_addr_hi
    eor bg_last_drawn_lo
    bne :+
    lda #0
    sta shouldDrawNewBg
    rts
:

    lda #1
    sta shouldDrawNewBg
    jmp renderBlankRow

renderThisBarrier:
    tax
    ; once we draw a barrier, don't draw a blank there
    lda bg_draw_addr_lo
    eor bg_draw_addr_hi
    sta bg_last_drawn_lo
    lda #1
    sta shouldDrawNewBg
    jmp renderBarrier

.endproc

.proc calcDrawAddress


   ;calc the address for new line

      ;clear the addr
      lda #0
      sta bg_draw_addr_hi

      ldx bg_scroll_hi
      ;sec
      ;sbc #32 ;why sub 32? for the header?
      ;bcs dontReset
      ;clc
      ;adc #240
;dontReset:

      ;divide scroll position by 8
      ;clc
      ;ror
      ;clc
      ;ror
      ;clc
      ;ror
      ;sta bg_draw_addr_lo
      ;pha
      ;;mult by 32

      ;first get us to the nearest 16
      inx
      txa
      and #%11110000
      clc
      adc #16

      ;reset if wrapped over 240
      cmp #240
      bcc :+
      sbc #240
:


      ;then divide by 8 to get the tile row
      and #%11111000
      clc
      ror
      ror
      ror

      ;save the row number for attr for later
      sta bg_draw_attr_lo

      sta bg_draw_addr_lo
      lda #$00
      sta bg_draw_addr_hi
    

      ;then multiply by 32 to get the actual draw address
      mult32 bg_draw_addr_lo,bg_draw_addr_hi

      lda bg_draw_addr_hi
      clc
      adc #$20
      sta bg_draw_addr_hi

      lda bg_draw_addr_hi
      lda bg_draw_addr_lo


      ;;;;;;;;;;;;;;;;;;;;;;;;;
      ; now set the palette address
      ;;;;;;;;;;;;;;;;;;;;;;;;;
      ;calc attribute addr
      lda bg_draw_attr_lo
      ;divide by 2 to get attr row
      clc
      ror
      ;then multiply by 8 for attr offset
      asl
      asl

      ;offset to attr mem is always C0
      clc
      adc #$C0
      sta bg_draw_attr_lo

      lda #$23
      sta bg_draw_attr_hi
      rts

.endproc

.proc renderBlankRow

 
    ;lda #$2C
    ;lda #$0C
    ;lda #$08
    ldy current_phase
    lda phase_bg_tiles,y
    sta phase_bg_tile


    ;put an offset that may get overwritten
    lda phase_bg_tile
    clc
    adc #3
    sta bg_draw_tile
    clc
    adc #$10
    sta bg_draw_tile+32

    ;blank space at first
    ;offset between 1 and 4
    jsr randomNumber

    and #1
    tax
:
    ;lda #$2C
    lda phase_bg_tile
    sta bg_draw_tile,x
    clc
    adc #$10
    sta bg_draw_tile+32,x
    inx
    cpx #32
    beq done

    ;carry is clear, so 1 off
    sec
    sbc #$F
    ;lda #$2D
    sta bg_draw_tile,x
    clc
    adc #$10
    ;lda #$3D
    sta bg_draw_tile+32,x
    inx
    cpx #32
    beq done

    sec
    sbc #$F
    ;lda #$2E
    sta bg_draw_tile,x
    clc
    adc #$10
    ;lda #$3E
    sta bg_draw_tile+32,x
    inx
    cpx #32
    beq done

    sec
    sbc #$F
    ;lda #$2F
    sta bg_draw_tile,x
    clc
    adc #$10
    ;lda #$3F
    sta bg_draw_tile+32,x
    inx
    cpx #32
    bne :-

done:
    rts
.endproc

.proc renderBarrier


temp_offset = 0
temp_size = 1
temp_x = 2
temp_right_start = 3
temp_tile = 4

    ;ldx #64
;:
    ;lda #BOX_TILE+1
    ;sta bg_draw_tile,x
    ;dex
    ;bpl :-
    ;rts

    ldy current_phase
    lda phase_wall_tiles,y
    sta temp_tile


    lda bg_barrier_opening_offset,x
    sta temp_offset
    lda bg_barrier_opening_size,x
    sta temp_size

    ;;;;;;;;;;;;;;;;;;;;;;;;;
    ;prepare the first barriers
    ;;;;;;;;;;;;;;;;;;;;;;;;
    ldx temp_offset
    
    ;if negative, fix it
      bpl :+
      ldx #0
      stx temp_offset
:



    beq prepareGap
:
    lda temp_tile
    clc
    adc #1
    sta bg_draw_tile,x
    adc #16
    sta bg_draw_tile+32,x
    dex
    bmi prepareGap
    lda temp_tile
    sta bg_draw_tile,x
    adc #16
    sta bg_draw_tile+32,x

    ;and the attr
    ;stx temp_x
    ;txa
    ;clc
    ;ror
    ;tax
    ;lda #BOX_ATTR_PAL
    ;sta bg_draw_attr,x
    ;ldx temp_x

    dex
    bpl :-

    ;;;;;;;;;;;;;;;;;;;;;;;;;
    ; prepare the gap
    ;;;;;;;;;;;;;;;;;;;;;;;;;
prepareGap:
    lda temp_offset
    clc
    adc temp_size
    and #%11111110 ;don't let temp_right_start be even?
    sta temp_right_start
    tax
:
    lda phase_bg_tile
    sta bg_draw_tile,x
    clc
    adc #$10
    sta bg_draw_tile+32,x
    dex
    bmi out
    sec
    sbc #$0F
    sta bg_draw_tile,x
    clc
    adc #$10
    sta bg_draw_tile+32,x

    dex

    txa
    cmp temp_offset
    bpl :-

out:

    ;;;;;;;;;;;;;;;;;;;;;;;;;
    ;prepare the second set of barriers
    ;;;;;;;;;;;;;;;;;;;;;;;;
    lda #30
    cmp temp_right_start
    beq afterRightHalf
    ldx #30
:
    lda temp_tile
    clc
    adc #1
    sta bg_draw_tile,x
    adc #16
    sta bg_draw_tile+32,x
    dex
    bmi afterRightHalf
    lda temp_tile
    sta bg_draw_tile,x
    adc #16
    sta bg_draw_tile+32,x

      ;and the attr
    stx temp_x
    txa
    clc
    ror
    tax
    lda #BOX_ATTR_PAL
    sta bg_draw_attr,x
    ldx temp_x

    dex
    txa
    cmp temp_right_start
    bne :-

afterRightHalf:

    lda temp_tile
    sta bg_draw_tile+31
    clc
    adc #16
    sta bg_draw_tile+63

    rts
 
  
.endproc


.proc testBackground
temp = 0
  ;lda bg_draw_addr_hi
  lda #$20
  sta PPUADDR
  lda #$00
  ;lda bg_draw_addr_lo
  sta PPUADDR
  lda bg_scroll_hi
  ldx #32
:
  lda #0
  sta PPUDATA
  dex
  bpl :-

  ;lda bg_draw_addr_hi
  lda #$20
  sta PPUADDR
  lda #$00
  ;lda bg_draw_addr_lo
  sta PPUADDR
  lda bg_scroll_hi
  clc
  ror
  clc
  ror
  clc
  ror
  sta temp
  lda #32
  sec
  sbc temp
  tax
  beq done
:
  lda #BOX_TILE
  sta PPUDATA
  dex
  bpl :-
  
done:

  rts
.endproc

.proc renderBackground

  ldy #STAR_TILE
  ;draw player stars for wins
  lda #$24
  sta PPUADDR
  lda #$41
  sta PPUADDR
  ldx player_wins
  dex
  bmi :++
:
  sty PPUDATA
  dex
  bpl :-
:
  
  
  lda #$24
  sta PPUADDR
  lda #$49
  sta PPUADDR
  ldx player_wins+1
  dex
  bmi :++
:
  sty PPUDATA
  dex
  bpl :-
:

  lda #$24
  sta PPUADDR
  lda #$51
  sta PPUADDR
  ldx player_wins+2
  dex
  bmi :++
:
  sty PPUDATA
  dex
  bpl :-
:


  lda #$24
  sta PPUADDR
  lda #$59
  sta PPUADDR
  ldx player_wins+3
  dex
  bmi :++
:
  sty PPUDATA
  dex
  bpl :-
:




  ;always draw the score displays
  lda #$24
  sta PPUADDR
  lda #$44
  sta PPUADDR
  lda bcdDisplay0
  sta PPUDATA
  lda bcdDisplay1
  sta PPUDATA
  lda bcdDisplay2
  sta PPUDATA

  lda #$24
  sta PPUADDR
  lda #$4C
  sta PPUADDR
  lda bcdDisplay0+1
  sta PPUDATA
  lda bcdDisplay1+1
  sta PPUDATA
  lda bcdDisplay2+1
  sta PPUDATA

  lda #$24
  sta PPUADDR
  lda #$54
  sta PPUADDR
  lda bcdDisplay0+2
  sta PPUDATA
  lda bcdDisplay1+2
  sta PPUDATA
  lda bcdDisplay2+2
  sta PPUDATA

  lda #$24
  sta PPUADDR
  lda #$5C
  sta PPUADDR
  lda bcdDisplay0+3
  sta PPUDATA
  lda bcdDisplay1+3
  sta PPUDATA
  lda bcdDisplay2+3
  sta PPUDATA

.IFDEF ENABLE_DEBUGGING

  lda #$24
  sta PPUADDR
  lda #$64
  sta PPUADDR
  lda bcdDisplay0+4
  sta PPUDATA
  lda bcdDisplay1+4
  sta PPUDATA
  lda bcdDisplay2+4
  sta PPUDATA

  lda #$24
  sta PPUADDR
  lda #$6C
  sta PPUADDR
  lda bcdDisplay0+5
  sta PPUDATA
  lda bcdDisplay1+5
  sta PPUDATA
  lda bcdDisplay2+5
  sta PPUDATA

  lda #$24
  sta PPUADDR
  lda #$74
  sta PPUADDR
  lda bcdDisplay0+6
  sta PPUDATA
  lda bcdDisplay1+6
  sta PPUDATA
  lda bcdDisplay2+6
  sta PPUDATA

  lda #$24
  sta PPUADDR
  lda #$7C
  sta PPUADDR
  lda bcdDisplay0+7
  sta PPUDATA
  lda bcdDisplay1+7
  sta PPUDATA
  lda bcdDisplay2+7
  sta PPUDATA

.endif

  ;see if we should draw bg updates
  lda shouldDrawNewBg
  beq done

  ;draw the buffered bg data for this frame
  lda bg_draw_addr_hi
  sta PPUADDR
  lda bg_draw_addr_lo
  sta PPUADDR
  ldx #0
:
  lda bg_draw_tile,x
  sta PPUDATA
  inx
  cpx #64
  bne :-


    ;SKIP FOR NOW -- maybe don't need this
    ; this is the per-phase palette change,
    ;but I think I'm fine with the current setup
  jmp done
    ;grab the phase palette and put 
    ;it into y
    ldx current_phase
    lda phase_bg_pal_num,x
    tay

    lda bg_draw_attr_hi
    sta PPUADDR
    lda bg_draw_attr_lo
    sta PPUADDR
    ldx #0
:
    ;lda bg_draw_attr,x
    ;lda #%10101010
    sty PPUDATA
    inx
    cpx #16
    bne :-

done:
  rts
.endproc



tileLookupTable:
  .byt 0
  .byt 4
  .byt 5

.proc drawScoreHeaders
row_num = 0
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; Start by clearing the first nametable
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;ldx #$20
  ;lda #$00
  ;ldy #$AA
  ;jsr ppu_clear_nt




  ;;;;;;;;;;;;;;;;;;;;;;;
  ; set up the player frames
  ; in the other nametable
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  lda #$24
  sta PPUADDR
  lda #$00
  sta PPUADDR
  ldy #3

playerFrameTop:

      ;top of the frames
      lda #16
      sta PPUDATA


      ldx #5
:
          lda #17
          sta PPUDATA
          dex
          bpl :-

      lda #18
      sta PPUDATA

      dey
      bpl playerFrameTop


  ;middle of frame
  ldy #3
playerFrameMiddleTop:

      lda #22
      sta PPUDATA


      ldx #5
:
          lda #2
          sta PPUDATA
          dex
          bpl :-

      lda #23
      sta PPUDATA

      dey
      bpl playerFrameMiddleTop


  ;middle of frame
  ldy #3
playerFrameMiddle:

      lda #22
      sta PPUDATA


      ;draw 0's or blanks
      ;depending on whether player in game
      lda #2
      ;ldx player_ingame,y
      ;beq :+
      ;lda #96
;:
      

      ldx #5
:
      
          sta PPUDATA
          dex
          bpl :-

      lda #23
      sta PPUDATA

      dey
      bpl playerFrameMiddle


  
  ;bottom of frame
  ldy #3
playerFrameBottom:

      ;top of the frames
      lda #19
      sta PPUDATA


      ldx #5
:
          lda #20
          sta PPUDATA
          dex
          bpl :-

      lda #21
      sta PPUDATA

      dey
      bpl playerFrameBottom



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; now palette data for scoreboards
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  lda #$27
  sta PPUADDR
  lda #$C0
  sta PPUADDR

  lda #0
  sta PPUDATA
  sta PPUDATA
  lda #%01010101
  sta PPUDATA
  sta PPUDATA
  lda #%10101010
  sta PPUDATA
  sta PPUDATA
  lda #%11111111
  sta PPUDATA
  sta PPUDATA

  enableMainScreen

  rts



.endproc

