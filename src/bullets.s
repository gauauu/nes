.include "nes.inc"
.include "global.inc"

BULLET_START_TILE = $1A

PLAYER_SHOOT_DELAY_FAST = 60


.segment "ZEROPAGE"


.segment "BSS"

bullet_loop_temp: .res 1
bullet_x:         .res NUM_BULLETS
bullet_ylo:       .res NUM_BULLETS
bullet_yhi:       .res NUM_BULLETS
bullet_velocity:  .res NUM_BULLETS

.segment "CODE"

.proc initBullets
  
  ldx #NUM_BULLETS-1
  lda #0
:
  sta bullet_velocity,x
  dex
  bpl :-
  rts

.endproc

.proc getPlayerBulletVelocity
  ;note: at this point, y is the bullet index
  ;x is player index. both should be preserved.
  lda #$35
  rts
.endproc

;x is player shooting
.proc attemptToShoot
bullet_offset = 9

  ;can't shoot if respawning
  lda player_status,x
  cmp #PLAYER_STATUS_NORMAL
  beq :+
    rts
:

  ;can't shoot in kamikaze round
  lda current_phase
  cmp #PHASE_KAMIKAZE
  bne :+
    rts
:

  ;don't let the player shoot too fast
  lda player_timer,x
  bne done

  ;look for a free bullet for this player

    ;find the base x for this player
    ;by multiplying x by 4
    txa
    asl
    tay

    ;loop through this player's bullets
    lda player_max_bullet,x
    tax
:
      ;find an inactive bullet
      lda bullet_velocity,y
      beq found
      iny
      dex
      bpl :-

   ;if not found, rts
done:
   rts

found:
  ;y is bullet index
  ;so x can stay player index
  ldx player_x_temp


  ;reset player bullet timer
  lda player_bullet_time,x
  sta player_timer,x

  ;get player bullet velocity
  jsr getPlayerBulletVelocity
  sta bullet_velocity,y

  ;get bullet width, divide by 2 
  ;to center it on player
  lda player_bullet_width,x
  clc
  ror
  sta bullet_offset
  
  ;position the bullet centered on player
  lda player_xhi,x
  clc
  adc #9 ;8 is half player, +1 so we don't
  sbc bullet_offset ;have to set carry
  sta bullet_x,y

  lda player_yhi,x
  sta bullet_yhi,y
  lda #0
  sta bullet_ylo,y

  ;if player is respawning, make live
  lda player_status,x
  eor #PLAYER_STATUS_RESPAWNING
  bne :+
    lda #PLAYER_STATUS_NORMAL
    sta player_status,x
:

rts

.endproc

.proc updateBullets
  ldx #NUM_BULLETS-1
  stx bullet_loop_temp
:
  jsr updateBullet
  dec bullet_loop_temp
  ldx bullet_loop_temp
  bpl :-
  rts

.endproc

.proc updateBullet
lo  = 0
hi  = 1

  lda bullet_velocity,x
  beq :+


  and #$F0
  clc
  ror
  ror
  ror
  ror
  sta hi

  lda bullet_velocity,x
  and #$0F
  clc
  asl
  asl
  asl
  sta lo

  subTwo16s lo,hi,{bullet_ylo,x},{bullet_yhi,x}

  ;potential optimization: a should
  ; already have bullet_yhi,x
  ; (left in for readability)

  ;now kill the bullet if reached top
  lda bullet_yhi,x
  cmp #24
  bcs :+
  lda #0
  sta bullet_velocity,x

:
  rts


.endproc

.proc preRenderBullets
  ldx #NUM_BULLETS-1
  stx bullet_loop_temp
:
  jsr preRenderBullet
  dec bullet_loop_temp
  ldx bullet_loop_temp
  bpl :-
  rts

.endproc



.proc preRenderBullet
tile         = 0
player_color = 1
draw_x       = 2
draw_y       = 3
bullet_width = 4

  ;make sure the bullet is livel before we draw
  lda bullet_velocity,x
  bne :+
  rts
:

  ;load bullet width for later
  playerFromBullet
  tay
  lda player_bullet_width,y
  sta bullet_width


  ldy #BULLET_START_TILE
  ;figure out what tile to draw
  lda timer
  and #1
  beq :+
  iny
:
  sty tile

  ;store the position to temp zero page
  lda bullet_x,x
  sta draw_x
  lda bullet_yhi,x
  sta draw_y
  
  ;save the color
  playerFromBullet
  sta player_color
  
  ;start drawing to oam
  writeSprite draw_x,draw_y,tile,player_color
  lda tile
  clc
  adc #16
  sta tile
  lda draw_y
  clc
  adc #8
  sta draw_y
  writeSprite draw_x,draw_y,tile,player_color


  lda bullet_width
  cmp #8
  beq done


    ;adjust the x
    lda draw_x
    clc
    adc #(BULLET_WIDE_WIDTH - BULLET_DEFAULT_WIDTH)
    sta draw_x

    ;first draw the bottom once 
    ;as we're setup for that
    writeSprite draw_x,draw_y,tile,player_color
    ;reset the y and tile
    lda tile
    sec
    sbc #16
    sta tile

    lda draw_y
    sec
    sbc #8
    sta draw_y

    ;then draw the top
    writeSprite draw_x,draw_y,tile,player_color

done:
  rts

.endproc
