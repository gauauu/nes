.include "nes.inc"
.include "global.inc"

.segment "BSS"

;inputs for the collision detection routine
collision_1_x:    .res 1
collision_1_y:    .res 1
collision_1_w:    .res 1
collision_1_h:    .res 1

collision_2_x:    .res 1
collision_2_y:    .res 1
collision_2_w:    .res 1
collision_2_h:    .res 1

;temporary computed storage
collision_1_y2:   .res 1
collision_1_x2:   .res 1
collision_2_y2:   .res 1
collision_2_x2:   .res 1


.segment "CODE"


.proc checkCollision

  ;calc right side of 1
  lda collision_1_x
  clc
  adc collision_1_w
  sta collision_1_x2

  ;compare with left of 2
  cmp collision_2_x
  bcc noCollision

  
  ;calc right side of 2
  lda collision_2_x
  clc
  adc collision_2_w
  sta collision_2_x2

  ;compare with left of 1
  cmp collision_1_x
  bcc noCollision


  ;calc bottom of 1
  lda collision_1_y
  clc
  adc collision_1_h
  sta collision_1_y2

  ;compare with top of 2
  cmp collision_2_y
  bcc noCollision
  
  ;calc bottom of 2
  lda collision_2_y
  clc
  adc collision_2_h
  sta collision_2_y2

  ;compare with top of 2
  cmp collision_1_y
  bcc noCollision

  lda #1
  rts

noCollision:
  lda #0
  rts

.endproc
