.include "nes.inc"
.include "global.inc"


FRAME_TL = 16
FRAME_TM = 17
FRAME_TR = 18

FRAME_ML = 22
FRAME_MM = 2
FRAME_MR = 23

FRAME_BL = 19
FRAME_BM = 20
FRAME_BR = 21


.segment "BSS"

frame_start_address:    .res 2
frame_pal:              .res 1
frame_x:                .res 1
frame_y:                .res 1
frame_width:            .res 1
frame_height:           .res 1

.segment "CODE"

;slow operation to draw a frame. assuming main screen
; layout to be written to 
;frame_x - x position
;frame_y - y position
;frame_pal  - pal #
;frame_width - width
;frame_height - height
.proc drawFrame
   

  ;first compute starting address
    ;lda #$24
    ;sta frame_start_address
    lda #$00
    sta frame_start_address+1

    ;first add in x 
    lda frame_x
    clc
    adc frame_start_address+1
    sta frame_start_address+1

    
    ;add in y * 32
    ldy frame_y
    beq skipY
    clc
  :
    lda #$20
    add16 frame_start_address+1,frame_start_address
    dey
    bne :-

skipY:


  ;now start drawing
    lda frame_start_address
    sta PPUADDR
    lda frame_start_address+1
    sta PPUADDR

    ;;;;;;;;;;;
    ; TOP
    ;;;;;;;;;;;;
    ;top-left corner
      lda #FRAME_TL
      sta PPUDATA
    ;top row (width - 2)
      ldx frame_width
      dex
      dex
      lda #FRAME_TM
  :
      sta PPUDATA
      dex
      bne :-
    ;top-right corner
      lda #FRAME_TR
      sta PPUDATA
      

    ;next line
    jsr frameIncrementLine

    ;;;;;;;;;;;;;;;;;;;;
    ;middle rows loop (height - 2):
    ;;;;;;;;;;;;;;;;;;;;;;
      ldy frame_height
      dey
      dey
:
      ;left edge
        lda #FRAME_ML
        sta PPUDATA

      ;middle (blank)
        ldx frame_width
        dex
        dex
        lda #FRAME_MM
:
        sta PPUDATA
        dex
        bne :-
      
      ;right edge
        lda #FRAME_MR
        sta PPUDATA

      jsr frameIncrementLine
      dey
      bne :--

    ;;;;;;;;;;;;;;;;;
    ; BOTTOM
    ;;;;;;;;;;;;;;;;;
      
    ;bottom-left corner
      lda #FRAME_BL
      sta PPUDATA
    ;bottom row (width - 2)
      ldx frame_width
      dex
      dex
      lda #FRAME_BM
:
      sta PPUDATA
      dex
      bne :-
    ;bottom-right corner
      lda #FRAME_BR
      sta PPUDATA

  ;;;;;;;;;;;;;;;;;;;;;
  ;palette atributes
  ;;;;;;;;;;;;;;;;;;;;;
  ;TOOD: first pass is
  ; assuming nice boundaries
  ; on 4x4 metatile borders

  ;first set up the attribute value
  ;repeated 4 times
  lda frame_pal
  asl
  asl
  ora frame_pal
  asl
  asl
  ora frame_pal
  asl
  asl
  ora frame_pal
  sta frame_pal


  ;start with base address
  lda #$27
  sta frame_start_address
  lda #$c0
  sta frame_start_address+1

  ;divide x by 4 for x offset
  lda frame_x
  clc
  ror
  clc
  ror

  ;add it into the target address
  add16 frame_start_address+1,frame_start_address

  ;load up y, divide by 4, then multipy by 8
  ;(could be optimized!)
  lda frame_y
  clc
  ror
  clc
  ror
  asl
  asl
  asl
  ;add it into address
  add16 frame_start_address+1,frame_start_address

  
  
  ;load up height, divide by 4
  lda frame_height
  clc
  ror
  ror
  tay


  ;prep frame height
  lda frame_width
  clc
  ror
  ror
  sta frame_width

  ;start drawing attribute lines
:
  lda frame_start_address
  sta PPUADDR
  lda frame_start_address+1
  sta PPUADDR

  ;draw a line
  ldx frame_width
  lda frame_pal
:
  sta PPUDATA
  dex
  bne :-

  ;calc address of next line
  lda #8
  add16 frame_start_address+1,frame_start_address
  dey
  bne :--


  rts
.endproc

.proc frameIncrementLine
    lda #$20
    add16 frame_start_address+1,frame_start_address
    lda frame_start_address
    sta PPUADDR
    lda frame_start_address+1
    sta PPUADDR
    rts
.endproc
