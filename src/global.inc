.ifndef GLOBAL_INC
.define GLOBAL_INC

.include "macros.s"

NUM_PLAYERS = 4

OAM = $0200
BARRIER_NONE = 255

STARS_TO_WIN = 3
;STARS_TO_WIN = 1

;ENABLE_DEBUGGING=1

PLAYER_STATUS_NORMAL      = 0
PLAYER_STATUS_EXPLODING   = 1
PLAYER_STATUS_RESPAWNING  = 2
PLAYER_STATUS_WAIT        = 3

PHASE_NORMAL  = 0
PHASE_BATTLE  = 1
PHASE_SPEED   = 2
PHASE_KAMIKAZE= 3
PHASE_SURVIAL = 4
PHASE_NARROW  = 5

BULLETS_PER_PLAYER = 2
NUM_BULLETS        = (BULLETS_PER_PLAYER * 4)

BATTLE_POINTS_PER_KILL = 3
SURVIVAL_POINTS_PER_KILL = 2

PLAYER_SHOOT_DELAY_SLOW = 120
PLAYER_SHOOT_DELAY_INC  = 40
PLAYER_SHOOT_DELAY_MIN  = 40
PLAYER_RESPAWN_SHOOT_DELAY = 20

BULLET_DEFAULT_WIDTH = 8
BULLET_WIDE_WIDTH = 20

;INITIAL_PHASE = 0
INITIAL_PHASE = PHASE_NORMAL

SFX_EXPLOSION = 0
SFX_POWERUP   = 1
SFX_VICTORY   = 2
SFX_WARNING   = 3

LINE_BREAK    = 255


; init.s
.global reset_handler

; main.s
.global main, nmi_handler, irq_handler
.global waitForVBlank
.globalzp cur_keys, new_keys, oam_used
.globalzp background_update_addr
.globalzp timer
.globalzp misc_addr_ptr
.global waitAndUpdateScroll
.global render,preRenderFrame,delayTimer
.global paused, pause_timer
.globalzp mid_scroll_y,mid_scroll_voodoo

; bg.s
.global midFrameScroll
.global updateBackground
.global renderBackground
.global preRenderBackground
.global initBackground
.global bg_draw_addr_lo
.global bg_draw_addr_hi
.global getBarrierAtA
.global bg_scroll_hi
.global checkPointAgainstBarrier
.global bg_new_barrier_in
.global bg_new_barrier_in_lo
.global bg_barrier_y
.global bg_barrier_opening_offset
.global bg_barrier_opening_size
.global bg_scroll_speed_lo,bg_scroll_speed_hi

; player.s
.global initPlayers
.global player_colors
.global move_player, preRenderPlayerSprite,updatePlayers
.global player_ai
.globalzp player_ingame
.globalzp player_yhi,player_xhi
.globalzp player_ylo,player_xlo
.globalzp player_score
.globalzp player_wins
.globalzp player_max_bullet
.globalzp player_x_temp
.globalzp player_timer
.globalzp player_status
.globalzp player_speed_lo,player_speed_hi
.globalzp player_bullet_width,player_bullet_time


;ai.x
.global runAi
.global ai_timer

; ppuclear.s
.global ppu_clear_nt, ppu_clear_oam, ppu_screen_on, ppu_screen_off

; pads.s
.global read_pads

; misc.s
.global randomNumber

;scoreboard
.global debugData
.global preRenderScoreboard
.global bcdDisplay0,bcdDisplay1,bcdDisplay2


;famitone
.global FamiToneInit
.global FamiToneSampleInit
.global FamiToneUpdate
.global FamiToneMusicStart
.global FamiToneMusicStop
.global FamiToneSfxInit
.global FamiToneSfxStart

;music
.global music_module,music_moduleb,music_modulec

;sfx2
.global sounds

;phases
.global generateBarrier
.global phase_winner
.global initPhase
.global checkIfPhaseComplete
.global nextPhase
.global updatePhaseMetrics
.global phase_bg_tile
.global phase_bg_tiles
.global phase_bg_color_1,phase_bg_color_2
.global phase_complete
.global phase_texts_hi,phase_texts_lo
.global phase_inst_hi,phase_inst_lo
.global phase_wall_tiles
.global phase_bg_pal_num
.global phase_almost_over
.globalzp current_phase

;phaseTitle
.global phaseTitle
.global phaseEnding
.global updateTitleSprites
.global preRenderTitleSprites
.global endingUpdateScroll

;bullet
.global initBullets
.global attemptToShoot
.global updateBullets
.global preRenderBullets
.global bullet_x
.global bullet_yhi
.global bullet_velocity

;collisionChecker
.global checkCollision
.global collision_1_x
.global collision_1_y
.global collision_1_w
.global collision_1_h
.global collision_2_x
.global collision_2_y
.global collision_2_w
.global collision_2_h

;title
.global titleScreen

;text 
.global drawText
.global drawTextCentered
.global drawTextLines
.global press_start,ready
.global pressa
.global pressatojoina,pressatojoinb
.global pressstartwhenalla,pressstartwhenallb,pressstartwhenallc
.global norm,battle,speed,danger,survival,narrow
.global normInst,battleInst,survivalInst,kamikazeInst,speedInst,narrowInst
.global credit0,credit1,credit2,credit3,credit4
.global winner
.global players
.global bestwith

;startScreen
.global startScreen

;drawFrame
.global frame_start_address
.global frame_pal
.global frame_x,frame_y
.global frame_width,frame_height
.global drawFrame

;powerUps
.global updatePowerups
.global initPowerups
.global preRenderPowerups
.global playerSpeedIncrease

;winnerScreen
.global showWinnerScreen

.endif


