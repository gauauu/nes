
.macro writeSprite m_draw_x,m_draw_y,m_tile,m_color

  ldx oam_used
  ;y
    lda m_draw_y
    sta OAM,x
  ;tile
    lda m_tile
    sta OAM+1,x
  ;flip/palette
    lda m_color
    sta OAM+2,x
  ;x
    lda m_draw_x
    sta OAM+3,x

  inx
  inx
  inx
  inx

  stx oam_used

.endmacro

.macro absoluteValue
  bpl :+
  eor #$ff
  clc; not needed if the state of C is known up front
  adc #1; would be "adc #0", if C is known to be set
:
.endmacro

.macro pushRegisters  
  php
  pha
  txa
  pha
  tya
  pha
.endmacro

.macro pullRegisters
  pla
  tay
  pla
  tax
  pla
  plp
.endmacro

.macro ceil var,maxValue
  lda maxValue
  cmp var
  bcs ceilEnd
    sta var
ceilEnd:
.endmacro

.macro ceil16 lo,hi,maxLo,maxHi
  ;make sure hi not greater
  lda maxHi
  cmp hi
  bcs ceil16eq
    sta hi

ceil16eq:

  ;now if high equal, check lo
  cmp hi
  bne ceil16End

    ceil lo,maxLo

ceil16End:

.endmacro

.macro floor var,minValue
  lda minValue
  cmp var
  bcc :+
    sta var
:
.endmacro

;adds A to lo,hi
.macro add16  lo,hi
  clc
  adc lo
  sta lo
  lda #0
  adc hi
  sta hi
.endmacro


.macro mult32 lo,hi
      ;multiply by 32
      ldx #4
:
        asl lo
        rol hi
        dex
        bpl :-
.endmacro

.macro mult64 lo,hi
      ;multiply by 64
      ldx #5
:
        asl lo
        rol hi
        dex
        bpl :-
.endmacro


;subtracts A from lo,hi
.macro sub16  lo,hi
  ;http://wiki.nesdev.com/w/index.php/Synthetic_instructions#Reverse_subtraction
  eor #$FF
  sec
  adc lo
  sta lo
  lda hi
  sbc #0
  sta hi
.endmacro

.macro playerFromBullet
  txa
  clc
  ror
.endmacro


;adds 1 to 2, storing result in 2
.macro addTwo16s  lo1,hi1,lo2,hi2
  lda lo1
  clc
  adc lo2
  sta lo2
  lda hi1
  adc hi2
  sta hi2
.endmacro


;calc 2-1, storing result in 2
.macro subTwo16s  lo1,hi1,lo2,hi2
  lda lo2
  sec
  sbc lo1
  sta lo2
  lda hi2
  sbc hi1
  sta hi2
.endmacro


.macro drawString textLabel,ppuhi,ppulo
  lda #<textLabel
  sta misc_addr_ptr
  lda #>textLabel
  sta misc_addr_ptr+1
  lda #ppulo
  ldx #ppuhi
  jsr drawText
.endmacro


;oops, the first doens't allow indexed,
;but I don't want to refactor it everywhere
; this is the same things as drawString
;but doesn't assume ppulo and ppuhi are absolute
;addressed, allows immediate addressed
.macro drawStringImmediate textLabel,ppuhi,ppulo
  lda #<textLabel
  sta misc_addr_ptr
  lda #>textLabel
  sta misc_addr_ptr+1
  lda ppulo
  ldx ppuhi
  jsr drawText
.endmacro

.macro drawFrameAt fx,fy,width,height,pal,ppu_hi
  lda ppu_hi
  sta frame_start_address
  lda fx
  sta frame_x
  lda fy
  sta frame_y
  lda width
  sta frame_width
  lda height
  sta frame_height
  lda pal
  sta frame_pal
  jsr drawFrame
.endmacro

.macro clearMainScreen
  ldx #$24
  lda #$00
  ldy #$AA
  jsr ppu_clear_nt
.endmacro

.macro enableMainScreen
  ; Turn the screen on
  ldx #0
  ldy #0
  lda #VBLANK_NMI|BG_0000|OBJ_1000|NT_2400
  sec
  jsr ppu_screen_on
.endmacro

.macro enableLeftScreen
  ldx #0
  ldy #0
  lda #VBLANK_NMI|BG_0000|OBJ_1000|NT_2000
  sec
  jsr ppu_screen_on
.endmacro

.macro enableLeftScreenAt yy
  ldx #0
  ldy yy
  lda #VBLANK_NMI|BG_0000|OBJ_1000|NT_2000
  sec
  jsr ppu_screen_on
.endmacro

.macro musicUpdate
  jsr FamiToneUpdate
.endmacro

.macro musicStop
  jsr FamiToneMusicStop
.endmacro


.macro playSfx sfxNum
  lda sfxNum
  ldx #0
  jsr FamiToneSfxStart
.endmacro

.macro signedDiv2 lo,hi
  lda hi       ;Load the MSB
  asl                  ;Copy the sign bit into C
  ror hi       ;And back into the MSB
  ror lo       ;Rotate the LSB as normal
.endmacro

.macro unsignedDiv2 lo,hi
  clc
  ror hi       ;And back into the MSB
  ror lo       ;Rotate the LSB as normal
.endmacro
