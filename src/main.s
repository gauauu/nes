; Simple sprite demo for NES
; Copyright 2011-2014 Damian Yerrick
;
; Copying and distribution of this file, with or without
; modification, are permitted in any medium without royalty provided
; the copyright notice and this notice are preserved in all source
; code copies.  This file is offered as-is, without any warranty.
;
; Heavily modified by Nathan Tolbert
; for Spacey McRacey

.include "nes.inc"
.include "global.inc"


.segment "ZEROPAGE"
nmis:          .res 1
oam_used:      .res 1  ; starts at 0
cur_keys:      .res 4
new_keys:      .res 4
timer:         .res 1
misc_addr_ptr: .res 2 ;misc for passing addresses to procs
background_update_addr: .res 2
mid_scroll_y:  .res 1
mid_scroll_voodoo:  .res 1

.segment "BSS"
delayTimer:    .res 1
song:          .res 1
paused:        .res 1
pause_timer:   .res 1

NUM_SONGS = 3

.segment "CODE"
;;
; This NMI handler is good enough for a simple "has NMI occurred?"
; vblank-detect loop.  But sometimes there are things that you always
; want to happen every frame, even if the game logic takes far longer
; than usual.  These might include music or a scroll split.  In these
; cases, you'll need to put more logic into the NMI handler.
.proc nmi_handler
  inc nmis
  rti
.endproc

; A null IRQ handler that just does RTI is useful to add breakpoints
; that survive a recompile.  Set your debugging emulator to trap on
; reads of $FFFE, and then you can BRK $00 whenever you need to add
; a breakpoint.
;
; But sometimes you'll want a non-null IRQ handler.
; On NROM, the IRQ handler is mostly used for the DMC IRQ, which was
; designed for gapless playback of sampled sounds but can also be
; (ab)used as a crude timer for a scroll split (e.g. status bar).
.proc irq_handler
  rti
.endproc

.proc main

  lda #0
  sta timer
  sta paused

  ;clear nametables and sprites
    jsr ppu_screen_off

    ;clear sprites
    ldx #0
    sta oam_used
    jsr ppu_clear_oam

    ;clear nametable/attributes
    ;ldx #$20
    ;lda #0
    ;ldy #0
    ;jsr ppu_clear_nt
    ;lda #0
    ;ldy #0
    ;ldx #$24
    ;jsr ppu_clear_nt
    ;lda #0
    ;ldy #0
    ;ldx #$28
    ;jsr ppu_clear_nt
    ;lda #0
    ;ldy #0
    ;ldx #$2C
    ;jsr ppu_clear_nt

    lda #$23
    sta PPUADDR
    lda #$C0
    sta PPUADDR
    ldx #255
:
    stx PPUDATA
    dex
    bpl :-

    lda #$27
    sta PPUADDR
    lda #$C0
    sta PPUADDR
    ldx #255
:
    stx PPUDATA
    dex
    bpl :-

    lda #$2B
    sta PPUADDR
    lda #$C0
    sta PPUADDR
    ldx #255
:
    stx PPUDATA
    dex
    bpl :-

    lda #$2F
    sta PPUADDR
    lda #$C0
    sta PPUADDR
    ldx #255
:
    stx PPUDATA
    dex
    bpl :-


    ldx #0
    lda #0
:
      sta $0000,x
      sta $0100,x
      sta $0200,x
      sta $0300,x
      sta $0400,x
      sta $0500,x
      sta $0600,x
      sta $0700,x
    dex
    bne :-


  lda #1
  jsr FamiToneInit

  ; Now the PPU has stabilized, and we're still in vblank.  Copy the
  ; palette right now because if you load a palette during forced
  ; blank (not vblank), it'll be visible as a rainbow streak.
  jsr load_main_palette

  ; While in forced blank we have full access to VRAM.
  ; Load the nametable (background map).
  ;jsr draw_bg

  lda #VBLANK_NMI
  sta PPUCTRL
  
  ldx #<sounds
  ldy #>sounds
  jsr FamiToneSfxInit

  ldx #<music_moduleb
  ldy #>music_moduleb
  jsr FamiToneMusicStart

  ;show the title
  jsr titleScreen


  ;show the start screen
  jsr waitForVBlank
  jsr load_main_palette

  jsr startScreen


  ;prep for the actual game
  jsr ppu_screen_off


; outside loop (for multiple phases)

  lda #INITIAL_PHASE
  jsr initPhase

phaseLoop:
  
  jsr waitForVBlank
  jsr initBackground
  jsr phaseTitle



  ;lda #6
  ;sta player_score
  lda song
  cmp #0
  bne :+
    ldx #<music_modulec
    ldy #>music_modulec
    lda #1
    bne afterSongSelect
:
  cmp #1
  bne :+
    ldx #<music_moduleb
    ldy #>music_moduleb
    lda #2
    bne afterSongSelect
:
  cmp #2
  bne :+
    ldx #<music_module
    ldy #>music_module
    lda #0
:
afterSongSelect:
  sta song
  jsr FamiToneMusicStart


forever:

  ;update game logic
  jsr read_pads

  lda paused
  beq :+
    jsr checkIfUnpause
    jmp afterUpdate
:
  jsr checkIfPause
  jsr updateFrame
afterUpdate:

  ;misc rendering-related
  jsr preRenderFrame
  jsr waitForVBlank
  jsr render
  lda paused
   
  ;when paused, use the alternative timing
  beq :+
     jsr endingUpdateScroll 
     jmp :++
:
  jsr waitAndUpdateScroll
:

  ;audio
  jsr FamiToneUpdate
  
  ;if phase almost over, 
  ;make music faster
  lda phase_almost_over
  beq :+
    lda timer
    and #1
    bne :+
      jsr FamiToneUpdate
:

  ;see if we're done
  lda phase_complete
  bne phaseDone
 
  jmp forever

phaseDone:
  jsr phaseEnding
  jsr nextPhase
  jmp phaseLoop

.endproc


.proc updateFrame
 

  ;increment game timer
  inc timer
  lda timer

 
  jsr updatePlayers
  jsr updateBullets
  jsr updatePowerups
  jsr updateBackground
  jsr updatePhaseMetrics
  jsr checkIfPhaseComplete

  rts
.endproc




.proc preRenderFrame
  ;special sprite 0 for mid-rendering scroll change
  lda #20 ;y
  sta OAM
  lda #3  ;tile
  sta OAM+1
  lda #0  ;flags
  sta OAM+2
  lda #1  ;x
  sta OAM+3


  ldx #4
  stx oam_used

  ; adds to oam_used
  ldx #3
playerDrawLoop:
  lda player_ingame,x
  beq next
  jsr preRenderPlayerSprite
next:
  dex
  bpl playerDrawLoop

  jsr preRenderBullets
  jsr preRenderPowerups

  ldx oam_used
  jsr ppu_clear_oam

  jsr preRenderBackground
  jsr preRenderScoreboard
  rts
.endproc



.proc waitForVBlank
  ; Good; we have the full screen ready.  Wait for a vertical blank
  ; and set the scroll registers to display it.
  lda nmis
vw3:
  cmp nmis
  beq vw3
  rts

.endProc
  



.proc render
  ; Copy the display list from main RAM to the PPU
  lda #0
  sta OAMADDR
  lda #>OAM
  sta OAM_DMA

  jsr renderBackground
  
  ; Turn the screen on
  ldx #0
  ldy #0
  lda #VBLANK_NMI|BG_0000|OBJ_1000|NT_2400
  sec
  jsr ppu_screen_on

  ;ldx #0
  ;ldy bg_scroll_hi
  ;lda #VBLANK_NMI|BG_0000|OBJ_1000|NT_2000
  ;sec
  ;jsr ppu_screen_on


  rts
.endproc


  
.proc waitAndUpdateScroll

  ;bg_scroll_hi is the top of the screen.
  ; but mid-frame scroll takes the CURRENT 
  ; scroll position, so add 32 (But check for wrapping)
  lda bg_scroll_hi

  cmp #208
  bcc :+
  sec
  sbc #240
:
  clc
  adc #32
afterReset:
  sta mid_scroll_y

  and #$F8
  clc
  asl
  asl
  sta mid_scroll_voodoo

 ;wait for the sprite 0 flag to clear
sprite0ClearWait:
  lda PPUSTATUS
  and #%01000000
  bne sprite0ClearWait
  
  ;wait for the sprite 0 flag to set
sprite1ClearWait:
  ;lda $2002
  ;and #%01000000
  ;beq sprite1ClearWait
  bit PPUSTATUS
  bvc sprite1ClearWait

  ldx #240
:
  dex
  bne :-
 

  jsr midFrameScroll
  rts
.endproc



.proc load_main_palette
  ; seek to the start of palette memory ($3F00-$3F1F)
  ldx #$3F
  stx PPUADDR
  ldx #$00
  stx PPUADDR
copypalloop:
  lda initial_palette,x
  sta PPUDATA
  inx
  cpx #32
  bcc copypalloop
  rts
.endproc

.proc checkIfPause
  ldx #3
loop:
    lda new_keys,x
    and #KEY_START
    beq :+
      lda #1
      sta paused
      lda #60
      sta pause_timer
      playSfx #SFX_POWERUP
      rts
:
  dex
  bpl loop
  rts
.endproc

.proc checkIfUnpause
  lda pause_timer
  bne pauseTimerStillActive

  ldx #3
loop:
    lda new_keys,x
    and #KEY_START
    beq :+
      lda #0
      sta paused
      lda #0
      sta pause_timer
      playSfx #SFX_POWERUP
      rts
:
  dex
  bpl loop
  rts

pauseTimerStillActive:
  dec pause_timer
  rts
.endproc

.segment "RODATA"
initial_palette:
  ;.byt $0F,$08,$16,$37,$0F,$08,$24,$37,$0F,$08,$1A,$37,$0F,$08,$12,$37
  .byt $0F,$0C,$16,$37,$0F,$0C,$24,$37,$0F,$0C,$1A,$37,$0F,$0C,$12,$37
  .byt $0F,$08,$16,$37,$0F,$08,$24,$37,$0F,$08,$1A,$37,$0F,$08,$12,$37

; Include the CHR ROM data
.segment "CHR"
  .incbin "obj/nes/bggfx.chr"
  .incbin "obj/nes/spritegfx.chr"
