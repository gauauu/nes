;
; NES controller reading code
; Copyright 2009-2011 Damian Yerrick
;
; Copying and distribution of this file, with or without
; modification, are permitted in any medium without royalty provided
; the copyright notice and this notice are preserved in all source
; code copies.  This file is offered as-is, without any warranty.
;

;
; 2011-07: Damian Yerrick added labels for the local variables and
;          copious comments and made USE_DAS a compile-time option
;

.export read_pads
.importzp cur_keys, new_keys
.import debugData

JOY1      = $4016
JOY2      = $4017



.segment "CODE"
.proc read_pads
firstRead = 0
secondRead = 2
thirdRead = 4
lastFrameKeys = 6

  ; store the current keypress state to detect key-down later
  lda cur_keys
  sta lastFrameKeys
  lda cur_keys+1
  sta lastFrameKeys+1
  lda cur_keys+2
  sta lastFrameKeys+2
  lda cur_keys+3
  sta lastFrameKeys+3




  ldx #1
  stx JOY1

  lda #0
  sta JOY1

  ldx #0
joyLoop:
  ldy #8
innerJoyLoop:
      lda JOY1  
      and #%00000011 ; ignore D2-D7
      cmp #1         ; CLC if A=0, SEC if A>=1
      rol firstRead,x   ; put one bit in the register
      lda JOY2       ; read player 2's controller the same way
      and #$03
      cmp #1
      rol firstRead+1,x
      dey
      bne innerJoyLoop
  inx
  inx
  txa
  cmp #6
  bne joyLoop

  lda thirdRead
  cmp #%00010000
  bne notFourPlayer

  lda thirdRead+1
  cmp #%00100000
  beq fourPlayer

notFourPlayer:
  ;clear third and fourth player controls
  lda #0
  sta firstRead+2
  sta firstRead+3

fourPlayer:

  ldx #3
:
  ;calc the new keys
  lda cur_keys,x
  eor #$FF
  and firstRead,x
  sta new_keys,x

  lda firstRead,x
  sta cur_keys,x
  dex
  bpl :-

  ;lda firstRead+1
  ;sta cur_keys+1

  ;lda firstRead+2
  ;sta cur_keys+2

  ;lda firstRead+3
  ;sta cur_keys+3

  rts

.endproc

