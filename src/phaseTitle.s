.include "nes.inc"
.include "global.inc"


.segment "RODATA"

.segment "BSS"

star_x_lo:   .res 1
star_x_hi:   .res 1
star_y_lo:   .res 1
star_y_hi:   .res 1
star_dx_lo:  .res 1
star_dx_hi:  .res 1
star_dy_lo:  .res 1
star_dy_hi:  .res 1
star_dest_x: .res 1
star_dest_y: .res 1
star_dx_neg: .res 1



.segment "CODE"

.proc phaseTitle
tmp = 0
  jsr setupPhaseTitle
phaseTitleLoop:
  jsr updatePhaseTitle
  musicStop
  musicUpdate

  ;loop for X seconds
  ldy #1
:
  ldx #60
:
    jsr waitForVBlank
    jsr renderPhaseTitle
    jsr updatePhaseTitle
    stx tmp
    musicUpdate
    ldx tmp
  dex
  bpl :-
  dey
  bpl :--

  jsr waitForVBlank
  jsr clearPhaseTitleText

  rts

.endproc



.proc setupPhaseTitle
row = 0

  ;turn off rendering
  jsr ppu_screen_off

  ;clear sprites
  ldx #0
  sta oam_used
  jsr ppu_clear_oam

  ;clear nametable/attributes
  ldx #$20
  lda #0
  ldy #0
  jsr ppu_clear_nt


  ;setup palette for phase
  ;;;;;;;;;;;;;;;;;;;;;;;;;
    ldx current_phase
    lda #PAL_BASE
    sta PPUADDR
    lda #PAL_BG
    sta PPUADDR

    ldy #0
:
    lda #$0F
    sta PPUDATA

    lda phase_bg_color_1,x
    sta PPUDATA

    lda player_colors,y
    sta PPUDATA

    lda phase_bg_color_2,x
    sta PPUDATA

    iny
    cpy #4
    bne :-
  

  ;get tile for phase
  ;;;;;;;;;;;;;;;;;;;;;;
    lda phase_bg_tiles,x
    sta phase_bg_tile


  ;fill primary screen with tiled background
  ;;;;;;;;;;;;;;;;;;;;;;
    lda #$20
    sta PPUADDR
    lda #$00
    sta PPUADDR

    lda #14
    sta row
rowLoop:

    ;write a row
    ldx #15
    ldy phase_bg_tile
    tya
    iny
:
    sta PPUDATA
    sty PPUDATA
    dex
    bpl :-
    
    ;write the other half of the row
    ldx #15
    lda phase_bg_tile
    clc
    adc #$10
    tay
    iny
:
    sta PPUDATA
    sty PPUDATA
    dex
    bpl :-

    ;repeat that 12 times
    dec row
    bpl rowLoop


  ;add text in middle
  ;(probably want a box also?)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;
    drawFrameAt #4,#4,#24,#16,#0,#SCREEN_LEFT
    

    ;draw phase name
    ldx current_phase
    lda phase_texts_lo,x
    sta misc_addr_ptr
    lda phase_texts_hi,x
    sta misc_addr_ptr+1
    ldx #$21
    lda #$4A
    jsr drawText

    ;draw instructions
    ldx current_phase
    lda phase_inst_lo,x
    sta misc_addr_ptr
    lda phase_inst_hi,x
    sta misc_addr_ptr+1
    ldx #$21
    lda #$88
    jsr drawTextLines



  ;position player sprites and show score
  ;turn on rendering
  enableLeftScreenAt #0
  rts
.endproc

.proc updatePhaseTitle
  ;maybe blink or something?
  rts
.endproc

.proc renderPhaseTitle
  ;render whatever we set up in the update part

  ;dma sprites
  lda #0
  sta OAMADDR
  lda #>OAM
  sta OAM_DMA


  rts
.endproc

.proc clearPhaseTitleText
  ;clear the text from the middle (Replace with proper tiling)
  ;clear the scoreboard
  rts
.endproc

.proc calcStarMovement

diff = 1

  ldx phase_winner
  lda player_xhi,x
  sta star_x_hi
  lda player_yhi,x
  sta star_y_hi

  ;find destination
  txa
  ;mult by 64
  clc
  asl;x2
  asl;x4
  asl;x8
  asl;x16
  asl;x32
  asl;x64
  ;then add 32?
  clc
  adc #24
  sta star_dest_x

  lda #16
  sta star_dest_y

  lda #0
  sta star_dy_lo
  sta star_dx_lo


  ;dy
  lda star_y_hi
  sbc star_dest_y
  sta star_dy_hi


  ;dx -- dealing with unsigned inputs but need a signed output? ugh
  lda star_dest_x
  cmp star_x_hi
  bcc negativeXDirection

positiveXDirection:
  lda #0
  sta star_dx_neg
  lda star_dest_x
  sbc star_x_hi
  sta star_dx_hi ;positive direction, everything is fine
  sta diff
  jmp findPerFrameDxy

negativeXDirection:
  lda #1
  sta star_dx_neg
  lda star_x_hi
  sbc star_dest_x
  sta diff
  sta star_dx_hi ;now we have it in positive terms, but will need to reverse it later


  ;now calc the amount to move per frame
findPerFrameDxy:

  ;divide movement by 64
  ldx #5
:
  unsignedDiv2 star_dx_lo,star_dx_hi
  unsignedDiv2 star_dy_lo,star_dy_hi
  dex
  bpl :-


  ;lda star_x_hi
  ;sta debugData
  ;lda star_dest_x
  ;sta debugData+1
  ;lda diff
  ;sta debugData+2
  ;lda star_dx_hi
  ;sta debugData+2
  ;lda star_dx_lo
  ;sta debugData+3
  ;lda star_dy_lo
  ;sta debugData+3
  ;lda star_dx_hi
  ;sta debugData+1
  ;lda star_dx_lo
  ;sta debugData+2

  ;lda #0
  ;sta star_dy_hi
  ;lda #100
  ;sta star_dy_lo

  rts

.endproc

.proc updateStar
  lda star_dx_neg
  beq positive
    subTwo16s star_dx_lo,star_dx_hi,star_x_lo,star_x_hi
    jmp :+
positive:
    addTwo16s star_dx_lo,star_dx_hi,star_x_lo,star_x_hi
:
  subTwo16s star_dy_lo,star_dy_hi,star_y_lo,star_y_hi
  rts
.endproc


  
.proc endingUpdateScroll
 ;wait for the sprite 0 flag to clear
sprite0ClearWait:
  lda PPUSTATUS
  and #%01000000
  bne sprite0ClearWait
  
  ;wait for the sprite 0 flag to set
sprite1ClearWait:
  lda $2002
  and #%01000000
  beq sprite1ClearWait

  ldx #228
:
  dex
  bne :-
 

  jsr midFrameScroll
  rts
.endproc



.proc phaseEnding


  ;first a bit of a delay
  lda #120
  sta delayTimer
:
  jsr preRenderFrame
  jsr waitForVBlank
  jsr render
  jsr endingUpdateScroll
  musicUpdate
  dec delayTimer
  bne :-


  jsr calcStarMovement
  ;then move the star
  lda #64
  ;lda #255
  sta delayTimer
:
  lda delayTimer
  and #%00000011
  ;bne skipUpdate
    jsr updateStar
skipUpdate:
  jsr preRenderFrame
  writeSprite star_x_hi,star_y_hi,#$44,#$0
  clc
  lda star_x_hi
  adc #8
  sta star_x_hi
  writeSprite star_x_hi,star_y_hi,#$45,#$0
  clc
  lda star_y_hi
  adc #8
  sta star_y_hi
  writeSprite star_x_hi,star_y_hi,#$55,#$0
  sec
  lda star_x_hi
  sbc #8
  sta star_x_hi
  writeSprite star_x_hi,star_y_hi,#$54,#$0
  sec
  lda star_y_hi
  sbc #8
  sta star_y_hi

  jsr waitForVBlank
  jsr render
  jsr endingUpdateScroll
  musicUpdate
  dec delayTimer
  beq :+
  jmp :-
:
  ldx phase_winner
  inc player_wins,x

  playSfx #SFX_POWERUP
  lda #90
  sta delayTimer
:
    jsr preRenderFrame
    jsr waitForVBlank
    jsr render
    jsr endingUpdateScroll
    musicUpdate
    dec delayTimer
  bpl :-
  
  ldx phase_winner
  lda player_wins,x
  cmp #STARS_TO_WIN
  bne :+
  jmp showWinnerScreen

:
  rts

.endproc



