.include "nes.inc"
.include "global.inc"

NUM_PHASES = 6
PHASE_SCORE = 100
PHASE_WARN  = 80

;PHASE_SCORE = 5
;PHASE_WARN  = 5
;PHASE_SCORE = 5
;PHASE_WARN  = 4


.segment "ZEROPAGE"
current_phase:  .res 1

.segment "BSS"
bg_barrier_last_offset: .res 1
generate_barrier_temp:  .res 3
phase_bg_tile:          .res 1
phase_complete:         .res 1
phase_winner:           .res 1
phase_internal_counter: .res 1
barrier_gap:            .res 1
last_direction:         .res 1
phase_almost_over:      .res 1

.segment "RODATA"


phase_bg_pal_num:
  .byte %0 ;normal
  .byte %0 ;battle
  .byte %0 ;speed
  ;.byte %11111111 ;kamkikaze
  .byte 0 ;kamikaze
  .byte 0 ;survival
  .byte %0 ;challenge/narrow
phase_barrier_respawn_delay: 
  .byte 60
  .byte 120
  .byte 50
  .byte 70
  .byte 70
  .byte 90
phase_barrier_gap:
  .byte 14
  .byte 20
  .byte 12
  .byte 8
  .byte 12
  .byte 4
phase_bg_tiles:
  .byte $2C ; dotted squares
  .byte $0C ; cave stones
  .byte $08 ; 
  .byte $20 ; dotted squares
  .byte $2C ; dotted squares again
  .byte $E0 ; blank
phase_wall_tiles:
  .byte $80 ;pipe
  .byte $82 ;brick
  .byte $84 ;speed arrow
  .byte $86 ;box with circle
  .byte $80 ;survival, same as 1
  .byte $6C
phase_bg_color_1:
  .byte $03
  .byte $0C
  .byte $03
  .byte $0C ;kamikaze
  .byte $0B
  .byte $02
phase_bg_color_2:
  .byte $3C 
  .byte $39
  .byte $30
  .byte $37
  .byte $3A ;survival
  .byte $30
phase_texts_lo:
  .byte <norm
  .byte <battle
  .byte <speed
  .byte <danger
  .byte <survival
  .byte <narrow
phase_texts_hi:
  .byte >norm
  .byte >battle
  .byte >speed
  .byte >danger
  .byte >survival
  .byte >narrow
phase_inst_lo:
  .byte <normInst
  .byte <battleInst
  .byte <speedInst
  .byte <kamikazeInst
  .byte <survivalInst
  .byte <narrowInst
phase_inst_hi:
  .byte >normInst
  .byte >battleInst
  .byte >speedInst
  .byte >kamikazeInst
  .byte >survivalInst
  .byte >narrowInst
phase_func_lo:
  .byte <updatePhaseNormal
  .byte <updatePhaseBattle
  .byte <updatePhaseSpeed
  .byte <updatePhaseKamikaze
  .byte <updatePhaseSurvival
  .byte <updatePhaseNarrow
phase_func_hi:
  .byte >updatePhaseNormal
  .byte >updatePhaseBattle
  .byte >updatePhaseSpeed
  .byte >updatePhaseKamikaze
  .byte >updatePhaseSurvival
  .byte >updatePhaseNarrow

.proc initPhase
  sta current_phase
  tax
  lda phase_barrier_gap,x
  sta barrier_gap

  lda #0
  sta phase_complete
  sta phase_internal_counter
  sta phase_almost_over

  lda #$BF
  sta bg_scroll_speed_lo
  lda #$02
  sta bg_scroll_speed_hi

  jsr initPowerups
  jsr initBullets
  jsr initPlayers

  ;do special setup for battle phase
  lda current_phase
  cmp #PHASE_BATTLE
  bne :+
    jmp initBattlePhase
:

  cmp #PHASE_SPEED
  bne :+
    jmp initSpeedPhase
:

  rts
.endproc

.proc updatePhaseMetrics
  ldx current_phase
  lda phase_func_lo,x
  sta misc_addr_ptr
  lda phase_func_hi,x
  sta misc_addr_ptr+1
  jmp (misc_addr_ptr)
.endproc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; DEFAULT PHASE CONSTANTS
DEFAULT_SCORE_THRESHHOLD = 25
DEFAULT_SPEED_INCREASE   = $20

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc updatePhaseNormal
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;SCORE_THRESHHOLD = 20
SCORE_THRESHHOLD = DEFAULT_SCORE_THRESHHOLD
SPEED_INCREASE   = DEFAULT_SPEED_INCREASE

  ;figure out score thressholds for updating it
  lda phase_internal_counter
  bne :+
  lda #SCORE_THRESHHOLD
  sta phase_internal_counter
:

  ;then see if any player has passed it
  ldx #NUM_PLAYERS-1
loop:
    ;if this player has passed it, update
    cmp player_score,x
    bcs afterChange
      ;reset internal counter
      adc #SCORE_THRESHHOLD ;we know carry is clear
      sta phase_internal_counter

      ;increase speed
      lda #SPEED_INCREASE
      add16 bg_scroll_speed_lo,bg_scroll_speed_hi
      ceil16 bg_scroll_speed_lo,bg_scroll_speed_hi,#$50,#4


      ;maybe decrease gap?
      ;jsr randomNumber
      ;and #%00000011
      ;bne afterChange
        ;dec barrier_gap
        ;floor barrier_gap,#6
afterChange:
  
  dex
  bpl loop

  rts
.endproc


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc updatePhaseBattle
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SCORE_THRESHHOLD = DEFAULT_SCORE_THRESHHOLD
SPEED_INCREASE   = DEFAULT_SPEED_INCREASE

  ;figure out score thressholds for updating it
  lda phase_internal_counter
  bne :+
  lda #SCORE_THRESHHOLD
  sta phase_internal_counter
:

  ;then see if any player has passed it
  ldx #NUM_PLAYERS-1
loop:
    ;if this player has passed it, update
    cmp player_score,x
    bcs afterChange
      ;reset internal counter
      adc #SCORE_THRESHHOLD ;we know carry is clear
      sta phase_internal_counter

      ;increase speed
      lda #SPEED_INCREASE
      add16 bg_scroll_speed_lo,bg_scroll_speed_hi
      ceil16 bg_scroll_speed_lo,bg_scroll_speed_hi,#$50,#4


      ;never decrease gap

afterChange:
  
  dex
  bpl loop
  rts
.endproc

.proc initBattlePhase
  ;load everyone up with tons of weaponry
  ldx #NUM_PLAYERS-1
  ldy #BULLET_WIDE_WIDTH
  lda #1
:
    sta player_max_bullet,x
    sty player_bullet_width,x
  dex
  bpl :-
  rts
.endproc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc updatePhaseSpeed
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  rts
.endproc

.proc initSpeedPhase
  ;give each player a couple speed increases
  ldx #NUM_PLAYERS-1
:
    inc player_speed_hi,x
    inc player_speed_hi,x
    inc player_speed_hi,x
    ;lda player_speed_lo,x
    ;adc #$80
    ;sta player_speed_lo,x
  dex
  bpl :-


  ;increase initial start speed
  inc bg_scroll_speed_hi
  inc bg_scroll_speed_hi
  ;inc bg_scroll_speed_hi
  lda bg_scroll_speed_lo
  adc #$C0
  sta bg_scroll_speed_lo

  rts
.endproc


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc updatePhaseKamikaze
  jmp updatePhaseNormal
.endproc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.proc updatePhaseSurvival
  rts
.endproc

.proc updatePhaseNarrow
  rts
.endproc



;increments the phase
.proc nextPhase

  ldx current_phase
  inx
  cpx #NUM_PHASES
  bne :+
  ldx #0
:
  txa
  jsr initPhase
  rts
.endproc

.proc checkIfPhaseComplete
  ;don't do things if already done
  lda phase_complete
  beq :+
  rts

  ;check to see who won
:
  ldx #NUM_PLAYERS-1
loop:

    lda #PHASE_SCORE
    cmp player_score,x
    beq complete  ;if exactly 100, complete
    bcs notComplete ;if less than 100, not complete
    ; (if more than 100, fall through and complete)

complete:
    ;if complete, mark finished
    lda #1
    sta phase_complete
    ;and save the winner
    stx phase_winner

    ;play victory
    musicStop
    playSfx #SFX_VICTORY
    jmp :+
    
notComplete:

    ;see if phase almost over
    lda phase_almost_over
    bne :+
    lda #PHASE_WARN
    cmp player_score,x
    bne :+
      lda #1
      sta phase_almost_over
      playSfx #SFX_WARNING
:
  
  dex
  bpl loop

  rts
.endproc

.proc generateBarrier
tmp   = 0
limit = 1

  lda barrier_gap
  sta bg_barrier_opening_size,x

  lda #32
  sec
  sbc barrier_gap
  sta limit

  ;figure out where to spawn it to line up with 16px boundaries
  ; of render-screen
  lda bg_scroll_hi
  clc
  adc #8
  and #%11110000 
  ; ok, we're on the by-16 boundary below the top of the screen
  ;(ie underneath the status bar)

  ;now convert it back to screen coordinates instead
  ; of nametable coordinates
  sec
  sbc bg_scroll_hi


  sta bg_barrier_y,x
  stx tmp

  ;find a number of -2,-1, 1, 2
  jsr randomNumber

  ;75% chance of continuing last direction
  pha
  and #%01100000
  beq newDirection
  pla
  lda last_direction

  ;if zero, though, no
  beq newDirectionAfterPla
  jmp directionPicked


newDirection:
  pla
newDirectionAfterPla:
  ldx tmp
  ror
  and #%00000011   ;now we have 0-3
  sec
  sbc #2  ;minus 2 is -2 to 1
  bne directionPicked
  ;if zero, make it 2
  lda #2

directionPicked:
  sta last_direction

  adc bg_barrier_last_offset ;add it to the last one

  ;make sure it's not too far left
  absoluteValue
  bne :+
    ;if left, set last_direction to right so it doesn't get stuck
    lda #1
    sta last_direction
:
  sta bg_barrier_opening_offset,x
  

  ;see if it's too far right
  cmp limit
  bcc :+
  lda limit
  sbc #1
  sta bg_barrier_opening_offset,x
  ;reset last direction to go left
  lda #254
  sta last_direction
:


  lda bg_barrier_opening_offset,x
  sta bg_barrier_last_offset
  ;make it an even number
  and #%11111110
  sta bg_barrier_opening_offset,x


  ;reset the respawn delay
  ldy current_phase
  lda phase_barrier_respawn_delay,y
  sta bg_new_barrier_in
  lda #0
  sta bg_new_barrier_in_lo

  rts
.endproc

