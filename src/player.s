.include "nes.inc"
.include "global.inc"

.segment "ZEROPAGE"
; Game variables


player_xlo:       .res 4  ; horizontal position is xhi + xlo/256 px
player_xhi:       .res 4

player_ylo:       .res 4
player_yhi:       .res 4


player_dxlo:      .res 4
player_dxhi:      .res 4
player_dylo:      .res 4
player_dyhi:      .res 4

player_timer:     .res 4

player_ingame:    .res 4
player_status:    .res 4

player_powerups:     .res 4
player_max_bullet:   .res 4
player_bullet_width: .res 4
player_bullet_time:  .res 4

player_score:     .res 4
player_wins:      .res 4

player_dir:       .res 4

player_speed_lo:  .res 4
player_speed_hi:  .res 4

player_x_temp:    .res 1

PLAYER_DIR_UP     = %00000001
PLAYER_DIR_DOWN   = %00000010
PLAYER_DIR_LEFT   = %00000100
PLAYER_DIR_RIGHT  = %00001000

PLAYER_Y_MIN      = 40

PLAYER_NORMAL_SPEED_LO = 180  ; speed limit in 1/256 px/frame
PLAYER_NORMAL_SPEED_HI = 1  ; speed limit in 1/256 px/frame
;WALK_ACCEL = 4  ; movement acceleration in 1/256 px/frame^2
;WALK_BRAKE = 8  ; stopping acceleration in 1/256 px/frame^2

POWERUP_SPEED = 0
POWERUP_MORE_BULLETS = 1

PLAYER_COLLISION_PADDING = 2
PLAYER_COLLISION_WIDTH = (16 - PLAYER_COLLISION_PADDING * 2)

PLAYER_TIMER_EXPLODING      = 30
PLAYER_SHIP_TILE            = $10
PLAYER_SHIP_TILE_RELOADING  = $1C
FIRST_EXPLOSION_TILE        = $12
SECOND_EXPLOSION_TILE       = (FIRST_EXPLOSION_TILE + 2)
THIRD_EXPLOSION_TILE        = (SECOND_EXPLOSION_TILE + 2)
KAMIKAZE_BARRIER_TILE       = $1E

LEFT_WALL = 32
RIGHT_WALL = 224


.segment "BSS"
player_ai:        .res 4


.segment "RODATA"


player_colors:
.byte $16
;.byte $24
.byte $04
.byte $1A
.byte $12


.segment "CODE"

.proc initPlayers
  ldx #NUM_PLAYERS-1
:
    lda #0
    sta player_xlo,x
    sta player_dxlo,x
    sta player_powerups,x
    sta player_score,x
    sta ai_timer,x

    lda #PLAYER_STATUS_RESPAWNING
    sta player_status,x

    lda #PLAYER_SHOOT_DELAY_SLOW
    sta player_bullet_time,x

    lda #0
    sta player_max_bullet,x

    lda #BULLET_DEFAULT_WIDTH
    sta player_bullet_width,x

    txa
    asl
    asl
    asl
    asl
    asl
    adc #64
    sta player_xhi,x
    lda #220
    sta player_yhi,x

    lda #PLAYER_NORMAL_SPEED_LO
    sta player_speed_lo,x
    lda #PLAYER_NORMAL_SPEED_HI
    sta player_speed_hi,x

  dex
  bpl :-

  rts
.endproc


.proc updatePlayers
  ;for each player, update them
  ldx #3
playerLoop:
    
    stx player_x_temp
    jsr updatePlayer
    ldx player_x_temp
    dex
  bpl playerLoop
  rts
.endproc

;;
; Moves the player character in response to controller 1.
; x is the player number
.proc updatePlayer
  lda player_ingame,x
  beq done

  lda player_ai,x
  beq :+
    jsr runAi
:

  lda player_status,x
  eor #PLAYER_STATUS_EXPLODING
  bne :+
  jmp updatePlayerExploding
:
  jsr updatePlayerVelocity
  jsr movePlayer
  
  lda player_timer,x
  beq :+
  dec player_timer,x
:

  lda cur_keys,x
  and #KEY_A
  beq :+
  jsr attemptToShoot
:

  ;done check collisions if respawning
  lda player_status,x
  cmp #PLAYER_STATUS_RESPAWNING
  beq done

    ;otherwise check collisions
    jsr checkPlayerBulletCollisions
    jsr checkPlayerWallCollisions
    jsr checkPlayerPlayerCollisions


done:
  rts
  
.endproc

.proc updatePlayerExploding
  dec player_timer,x
  bne :+
  lda #PLAYER_STATUS_RESPAWNING
  sta player_status,x
  lda #210
  sta player_yhi,x
  lda #0
  sta player_ylo,x
:
  rts
.endproc

.proc checkPlayerPlayerCollisions
  
  ldx player_x_temp

  lda player_yhi,x
  clc
  adc #PLAYER_COLLISION_PADDING
  sta collision_1_y

  lda player_xhi,x
  clc
  adc #PLAYER_COLLISION_PADDING
  sta collision_1_x

  lda #PLAYER_COLLISION_WIDTH
  sta collision_1_w
  sta collision_1_h
  sta collision_2_w
  sta collision_2_h


  ldy #NUM_PLAYERS-1
:
    ;don't collide with non-ingame players
    lda player_ingame,y
    beq noCollision

    ;don't collide with respawning or out of game
    lda player_status,y
    cmp #PLAYER_STATUS_NORMAL
    bne noCollision

    ;only actually compare if the other is above 
    ;(also skip self)
    lda player_yhi,y
    cmp player_yhi,x
    beq noCollision
    bcs noCollision

    clc
    adc #PLAYER_COLLISION_PADDING
    sta collision_2_y

    lda player_xhi,y
    adc #PLAYER_COLLISION_PADDING
    sta collision_2_x

    jsr checkCollision
    beq noCollision

    ;if we're in the kamikaze round,
    ;kill the player on top
    lda current_phase
    cmp #PHASE_KAMIKAZE
    bne noKamikaze
      ;save y, transfer x to y
      tya
      pha
      txa
      tay
      ;so we can award player x
      jsr possiblyAwardBattlePoints
      ;then restore y
      pla
      tay
      jsr killPlayerY
      jmp noCollision
noKamikaze:

    ;collision!
    jsr possiblyAwardBattlePoints
    jmp killActivePlayer

noCollision:
  dey
  bpl :-
.endproc

.proc checkPlayerBulletCollisions

  ldx player_x_temp

  ;set the player to the collision inputs
  lda player_xhi,x
  clc
  adc #PLAYER_COLLISION_PADDING
  sta collision_1_x
  lda player_yhi,x
  adc #PLAYER_COLLISION_PADDING
  sta collision_1_y
  lda #PLAYER_COLLISION_WIDTH
  sta collision_1_w
  sta collision_1_h

  ;set bullet height dimensions to collision checker
  lda #12
  sta collision_2_h

  ldx #(NUM_BULLETS - 1)
:
  ;if the bullet not active, don't check
  lda bullet_velocity,x
  beq noCollision

  ;if the bullet belongs to this player, skip
  playerFromBullet

  cmp player_x_temp
  beq noCollision

  ;get the bullet width and send to collision checker input
  tay
  lda player_bullet_width,y
  sta collision_2_w

  ;set the bullet to the collision checker inputs
  lda bullet_x,x
  sta collision_2_x
  lda bullet_yhi,x
  sta collision_2_y

  ;see if collision
  jsr checkCollision

  beq noCollision
  jmp playerBulletCollision

noCollision:
  dex
  bpl :-
  ldx player_x_temp

  rts

.endproc

.proc checkPlayerWallCollisions
xHiTemp = 0
yHiTemp = 1

  ;for now, only do collisions with first player
  ; for testing
  ;sec
  ;cpx #0
  ;bne noCollision

  lda player_status,x
  eor #PLAYER_STATUS_RESPAWNING
  beq noCollision

  lda player_xhi,x
  clc
  adc #PLAYER_COLLISION_PADDING
  sta xHiTemp

  lda player_yhi,x
  clc
  adc #PLAYER_COLLISION_PADDING
  sta yHiTemp

  ;if we go too high, limit to 32
  lda player_yhi,x
  cmp #PLAYER_Y_MIN
  bcs :+
  lda #PLAYER_Y_MIN
  sta player_yhi,x
:

  ;get the barrier at top of player
  jsr getBarrierAtA
  tax ;hold onto it in x for now, required by checkPointAgainstBarrier
  eor #BARRIER_NONE
  beq notTopCollision 
  lda xHiTemp ;a has position to check
  ldy #PLAYER_COLLISION_WIDTH ;pixel-wide character to check
  jsr checkPointAgainstBarrier
  bne collision


notTopCollision:

  ;check bottom. this could be optimized as it's nearly
  ; the same as above
  lda yHiTemp
  clc
  adc #PLAYER_COLLISION_WIDTH
  jsr getBarrierAtA
  tax ;hold onto it in x for now, required by checkPointAgainstBarrier
  eor #BARRIER_NONE
  beq noCollision 
  lda xHiTemp ;a has position to check
  ldy #16 ;16 pixel-wide character to check
  jsr checkPointAgainstBarrier
  bne collision

noCollision:
  rts

collision:
  jmp killActivePlayer

.endproc

.proc possiblyAwardBattlePoints

  lda current_phase
  cmp #PHASE_BATTLE
  beq awardPoints
  cmp #PHASE_KAMIKAZE
  bne :+

awardPoints:
  lda player_score,y
  clc
  adc #BATTLE_POINTS_PER_KILL
  sta player_score,y
:
  rts
.endproc

.proc playerBulletCollision

  ;if it's battle round, award points
  playerFromBullet
  tay
  jsr possiblyAwardBattlePoints

  ;kill the bullet
  lda #0
  sta bullet_velocity,x

  jmp killActivePlayer

.endproc


.proc killActivePlayer
  ;kill the hit player
  ldx player_x_temp
  lda #PLAYER_STATUS_EXPLODING
  sta player_status,x
  lda #PLAYER_TIMER_EXPLODING
  sta player_timer,x

  playSfx #SFX_EXPLOSION

  ;for survival phase, give everyone
  ;else points...
  lda current_phase
  cmp #PHASE_SURVIAL
  bne :+
    jsr awardNonActivePlayers
:
  rts
.endproc

.proc awardNonActivePlayers
  ldy #NUM_PLAYERS-1
:
    cpy player_x_temp
    beq loopBottom

    lda player_status,y
    cmp #PLAYER_STATUS_NORMAL
    bne loopBottom
    
    lda player_score,y
    clc
    adc #SURVIVAL_POINTS_PER_KILL
    sta player_score,y
    
loopBottom:
  dey
  bpl :-
  rts
.endproc

.proc killPlayerY
  lda #PLAYER_STATUS_EXPLODING
  sta player_status,y
  lda #PLAYER_TIMER_EXPLODING
  sta player_timer,y

  playSfx #SFX_EXPLOSION
  rts
.endproc


.proc updatePlayerVelocity

  lda #0
  sta player_dxhi,x
  sta player_dyhi,x
  sta player_dylo,x
  sta player_dxlo,x
  sta player_dir,x

  lda cur_keys,x
  and #KEY_RIGHT
  beq notRight
  lda player_speed_lo,x
  sta player_dxlo,x
  lda player_speed_hi,x
  sta player_dxhi,x
  lda #PLAYER_DIR_RIGHT
  sta player_dir,x

notRight:

  lda cur_keys,x
  and #KEY_LEFT
  beq notLeft
  lda player_speed_lo,x
  sta player_dxlo,x
  lda player_speed_hi,x
  sta player_dxhi,x
  lda #PLAYER_DIR_LEFT
  sta player_dir,x

notLeft:

  lda cur_keys,x
  and #KEY_UP
  beq notUp
  lda player_speed_lo,x
  sta player_dylo,x
  lda player_speed_hi,x
  sta player_dyhi,x
  lda player_dir,x
  ora #PLAYER_DIR_UP
  sta player_dir,x

  ;if we're respawning, go to normal
  lda player_status,x
  eor #PLAYER_STATUS_RESPAWNING
  bne notUp
  lda #PLAYER_STATUS_NORMAL
  sta player_status,x
  lda #PLAYER_RESPAWN_SHOOT_DELAY
  sta player_timer,x


notUp:

  lda cur_keys,x
  and #KEY_DOWN
  beq notDown
  lda player_speed_lo,x
  sta player_dylo,x
  lda player_speed_hi,x
  sta player_dyhi,x
  lda player_dir,x
  ora #PLAYER_DIR_DOWN
  sta player_dir,x

notDown:

  rts

.endproc

.proc movePlayer
  lda player_dir,x
  tay
  and #PLAYER_DIR_DOWN
  beq notDown
  addTwo16s {player_dylo,x},{player_dyhi,x},{player_ylo,x},{player_yhi,x}

  ;dont wrap off bottom
  cmp #220
  bcc notDown
  lda #220
  sta player_ylo,x
  sta player_yhi,x

notDown:

  tya
  and #PLAYER_DIR_UP
  beq notUp
  subTwo16s {player_dylo,x},{player_dyhi,x},{player_ylo,x},{player_yhi,x}

notUp:

  tya
  and #PLAYER_DIR_LEFT
  beq notLeft
  subTwo16s {player_dxlo,x},{player_dxhi,x},{player_xlo,x},{player_xhi,x}

  ;don't wrap left
  bcs notLeft
  lda #0
  sta player_xlo,x
  sta player_xhi,x

notLeft:

  tya
  and #PLAYER_DIR_RIGHT
  beq notRight
  addTwo16s {player_dxlo,x},{player_dxhi,x},{player_xlo,x},{player_xhi,x}

  ;don't wrap right
  cmp #244
  bcc notRight
  lda #244
  sta player_xlo,x
  sta player_xhi,x


notRight:

  rts
.endproc

;;
; Draws the player's character to the display list as six sprites.
; In the template, we don't need to handle half-offscreen actors,
; but a scrolling game will need to "clip" sprites (skip drawing the
; parts that are offscreen).
.proc preRenderPlayerSprite
draw_y        = 0
player_color  = 1
draw_x        = 2
tile          = 3
;cur_tile = 1
;x_add = 2         ; +8 when not flipped; -8 when flipped
;rows_left = 4
;row_first_tile = 5
;draw_x_left = 7

  ;if faded and every other frame, don't draw
  lda player_status,x
  eor #PLAYER_STATUS_RESPAWNING
  bne :+
  lda timer
  and #1
  beq :+
  rts

:

  ;figure out what tile to draw
  ldy #PLAYER_SHIP_TILE ;by default, draw spaceship

  ;but if we're exploding, use something else
  lda player_status,x
  eor #PLAYER_STATUS_EXPLODING
  bne afterExplosionCheck

  ldy #THIRD_EXPLOSION_TILE
  lda player_timer,x
  cmp #20
  bcc firstOrSecondFrame
  ldy #FIRST_EXPLOSION_TILE
  bne afterFrameSelected

firstOrSecondFrame:
  cmp #10
  bcc afterFrameSelected
  ldy #SECOND_EXPLOSION_TILE
  jmp afterFrameSelected

afterExplosionCheck:


  ;if we're reloading, switch
  lda player_timer,x
  beq afterFrameSelected
  ldy #PLAYER_SHIP_TILE_RELOADING

afterFrameSelected:

  sty tile

  lda player_xhi,x
  sta draw_x
  lda player_yhi,x
  sta draw_y


  txa
  tay
  sta player_color

  ldx oam_used

;y
  lda draw_y
  sta OAM,x
;tile
  lda tile
  sta OAM+1,x
;flipped/palette?
  lda player_color
  sta OAM+2,x
;x position
  lda draw_x
  sta OAM+3,x

  inx
  inx
  inx
  inx

  inc tile
;y
  lda draw_y
  sta OAM,x
;tile
  lda tile
  sta OAM+1,x
;flipped/palette?
  lda player_color
  sta OAM+2,x
;x position
  lda draw_x
  clc
  adc #8
  sta OAM+3,x

  inx
  inx
  inx
  inx

  lda tile
  clc
  adc #15
  sta tile

;y
  lda draw_y
  clc
  adc #8
  sta OAM,x
;tile
  lda tile
  sta OAM+1,x
;flipped/palette?
  lda player_color
  sta OAM+2,x
;x position
  lda draw_x
  sta OAM+3,x

  inx
  inx
  inx
  inx

  inc tile

;y
  lda draw_y
  clc
  adc #8
  sta OAM,x
;tile
  lda tile
  sta OAM+1,x
;flipped/palette?
  lda player_color
  sta OAM+2,x
;x position
  lda draw_x
  clc
  adc #8
  sta OAM+3,x

  inx
  inx
  inx
  inx

  stx oam_used


  ;if kamikaze round, render the barrier
  lda current_phase
  cmp #PHASE_KAMIKAZE
  bne :+
    tya
    tax
    lda player_status,x
    cmp #PLAYER_STATUS_NORMAL
    bne :+

      ;cycle colors
      lda timer
      ror
      ror
      and #%00000011
      sta player_color

      ;calc y from last drawn (bottom-right)
      lda draw_y
      sec
      sbc #2
      sta draw_y

      writeSprite draw_x,draw_y,#KAMIKAZE_BARRIER_TILE,player_color

      lda draw_x
      clc
      adc #8
      sta draw_x

      writeSprite draw_x,draw_y,#KAMIKAZE_BARRIER_TILE+1,player_color


:

  tya
  tax

  rts
 
.endproc

