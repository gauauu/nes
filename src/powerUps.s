.include "nes.inc"
.include "global.inc"

POWERUP_TILE     = 4

POWERUP_Y        = 35

POWERUP_INACTIVE = 0
POWERUP_SPEED    = 1
POWERUP_BULLETS  = 2
POWERUP_SPREAD   = 3
POWERUP_POINTS   = 4

SPEED_INCREMENT = 80

;POWERUP_10       = 5?
;POWERUP_SLOW     = 6?
;mines? glowy/collision?

NUM_POWERUPS = 2

.segment "BSS"

powerup_x:    .res NUM_POWERUPS
powerup_dir:  .res NUM_POWERUPS
powerup_type: .res NUM_POWERUPS
powerup_timer:.res 1

.segment "RODATA"

powerup_tiles:
  .byte 0
  .byte 10
  .byte 10
  .byte 10



.segment "CODE"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc initPowerups
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  lda #3 
  sta powerup_timer
  lda #0
  sta powerup_type
  sta powerup_type+1

  rts
.endproc


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc updatePowerups
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  jsr seeIfSpawnPowerup
  jsr updateActivePowerups
  jsr checkPowerupCollisions
  rts
.endproc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc seeIfSpawnPowerup
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;depends on phase maybe?
  ;counter for how long since spawn,
  ;after counter, random chance of spawning?
  ;(or random amount of time maybe better?)
  lda timer
  and #%000111111
  ;and #%000001111
  bne noSpawn

  dec powerup_timer
  bmi :+    ;if somehow negative, go ahead and spawn
  bne noSpawn
:


  jsr spawnPowerup

noSpawn:
  rts
.endproc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc spawnPowerup
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

tmp = 0
  ;find an unused powerup
  ldx #(NUM_POWERUPS-1)
:
  lda powerup_type,x
  bne notThisOne
  jmp found
notThisOne:
  dex
  bpl :-

  ;if didn't find one, reset powerup timer
  jsr randomNumber
  and #%00001111
  sta powerup_timer

  rts

found:
  stx tmp

  ;pick a side 
  jsr randomNumber
  ldx tmp
  and #%00000001
  beq rightSide

  ;left side:
  lda #1
  sta powerup_x,x
  sta powerup_dir,x
  jmp pickType

  ;right side:  
rightSide:
  lda #0
  sta powerup_dir,x
  lda #240
  sta powerup_x,x

  ;pick a type (based on phase?)
pickType:
  ;for now, just between 1 and 4
  jsr randomNumber
  pha
  ldx tmp
  ;and #%00011111
  and #%00011111
  ora #%00000001
  sta powerup_timer ;(also repurpose as random timer)
  pla
  ror
  ror
  ror
  and #%00000011
  clc
  adc #1
  sta powerup_type,x
  tay

  lda #5
  sta powerup_timer

  jmp cancelPowerupIfNotAllowedInPhase

done:
  ;spawn it
  rts
.endproc


.proc cancelPowerupIfNotAllowedInPhase
;Y has powerup type
;X has powerup index


  ;if not allowed in this phase, cancel it.
  ;might be able to replace this with some tables.
  ; but this will do for now

  lda current_phase
  cmp #PHASE_KAMIKAZE
  beq cancelPowerup

  cmp #PHASE_BATTLE
  bne notBattle
      cpy #POWERUP_BULLETS
      beq cancelPowerup
      cpy #POWERUP_SPREAD
      beq cancelPowerup
      rts
notBattle:

  cmp #PHASE_SPEED
  bne notSpeed
      cpy #POWERUP_SPEED
      beq cancelPowerup
      rts
notSpeed:

  rts

cancelPowerup:
    lda #0
    sta powerup_type,x
  rts

.endproc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc updateActivePowerups
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;make slow movement based on game timer
  lda timer
  and #%00000011
  beq :+
  rts
:

  ldx #(NUM_POWERUPS-1)
:

    ;check each powerup, skip if inactive
    lda powerup_type,x
    beq skipUpdate

    ;if active, move it
    lda powerup_dir,x
    bne moveLeft

    ;move right
    dec powerup_x,x
    beq removePowerup
    jmp skipUpdate

moveLeft:
    inc powerup_x,x
    beq removePowerup

skipUpdate:
  dex
  bpl :-
  rts

  ;remove the powerup if it went offscreen
removePowerup:
  lda #0
  sta powerup_type,x
  jmp skipUpdate

.endproc



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc checkPowerupCollisions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
bullet_width = 9

  ;check if bullets collide with powerup
  ldx #NUM_BULLETS - 1
bulletLoop:

    ;first check bullets so we can avoid 
    ;unnecessary checks

    ;is the bullet active?
    lda bullet_velocity,x
    beq noCollision

    ;is it high enough?
    lda bullet_yhi,x
    cmp #POWERUP_Y
    bcs noCollision

    ;load the bullet width for later
    playerFromBullet
    tay
    lda player_bullet_width,y
    sta bullet_width


    ;at this point, we're checking any active bullet
    ; at the right Y value against whatever

    ldy #NUM_POWERUPS-1
:
      lda powerup_type,y
      beq nextPowerup

      ;check the x values within each other
      lda bullet_x,x
      clc
      adc bullet_width;check right side of bullet
      cmp powerup_x,y ;compare to left of powerup
      bcc nextPowerup ;if less, no collision

      lda powerup_x,y
      clc
      adc #16         ;check right side of powerup
      cmp bullet_x,x  ;compare against left of bullet
      bcc nextPowerup ;if less, no collision

      jsr playerShotPowerup

nextPowerup:
      dey
      bpl :-

noCollision:
  dex
  bpl bulletLoop

  rts
.endproc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc playerShotPowerup
; x is bullet index
; y is powerup index
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;first disable the bullet
  lda #0
  sta bullet_velocity,x
  sta bullet_x,x

  ;put player in x instead of bullet
  playerFromBullet
  tax

  
  lda powerup_type,y
  cmp #POWERUP_SPEED
  beq acquirePowerupSpeed
  
  cmp #POWERUP_BULLETS
  beq acquirePowerupBullets

  cmp #POWERUP_SPREAD
  beq acquirePowerupSpread

  cmp #POWERUP_POINTS
  beq acquirePowerupPoints


killPowerupAndDone:
  lda #0
  sta powerup_type,y
  playSfx #SFX_POWERUP
  rts

acquirePowerupSpeed:
  jsr playerSpeedIncrease
  jmp killPowerupAndDone

acquirePowerupBullets:
  ;set to allow > 1 on screen
  ;(maybe deprecated?)
  lda player_max_bullet,x
  cmp #1
  beq :+
  inc player_max_bullet,x
:


  ;decrease player_bullet_time but...
  lda player_bullet_time,x
  sec
  sbc #PLAYER_SHOOT_DELAY_INC
  sta player_bullet_time,x

  ;don't go under minimum
  cmp #PLAYER_SHOOT_DELAY_MIN
  bcs :+
  lda #PLAYER_SHOOT_DELAY_MIN
  sta player_bullet_time,x


:
  jmp killPowerupAndDone

acquirePowerupSpread:
  lda #BULLET_WIDE_WIDTH
  sta player_bullet_width,x
  jmp killPowerupAndDone

acquirePowerupPoints:
  lda player_score,x
  clc
  adc #10
  sta player_score,x
  jmp killPowerupAndDone

.endproc

.proc playerSpeedIncrease
  lda #SPEED_INCREMENT
  add16 {player_speed_lo,x},{player_speed_hi,x}
  lda player_speed_hi,x
  cmp #3
  bcc :+
  lda #3
  sta player_speed_hi,x
  lda #0
  sta player_speed_lo,x
:
  rts
.endproc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc preRenderPowerups
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;draw powerups to oam
  ldx #(NUM_POWERUPS-1)
:
    lda powerup_type,x
    beq skip
    jsr preRenderPowerup
skip:
  dex
  bpl :-

  rts
.endproc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.proc preRenderPowerup
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
tmpX   = 3
tmpPal = 4
tile   = 5

  lda timer
  ror
  ror
  ror
  and #%00000011
  sta tmpPal

  ;figure out what tile to start with
  lda powerup_type,x
  asl
  clc
  adc #POWERUP_TILE
  sta tile


  txa
  tay
  writeSprite {powerup_x,y},#POWERUP_Y,tile,tmpPal

  lda powerup_x,y
  clc
  adc #8
  sta tmpX

  inc tile

  writeSprite tmpX,#POWERUP_Y,tile,tmpPal
  tya
  tax
  rts

 
.endproc
