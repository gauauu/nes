.include "nes.inc"
.include "global.inc"

.segment "BSS"


bcd_in:       .res    1
bcd_out:      .res    2
debug_temp:   .res    1

debugData:    .res 6

bcdDisplay0: .res 8
bcdDisplay1: .res 8
bcdDisplay2: .res 8
bcdDisplay3: .res 8


.segment "CODE"

.proc preRenderScoreboard


  ;first process the debug data
  ldx #7
:
  lda debugData-4,x
  jsr debugAatX
  dex
  cpx #3
  bne :-

  ;then the player scores
  ;ldx #3
:
  lda player_ingame,x
  bne handlePlayerScore
  lda #2 ;blank colored tile
  sta bcdDisplay0,x
  sta bcdDisplay1,x
  sta bcdDisplay2,x
  sta bcdDisplay3,x
  jmp playerScoreDex


handlePlayerScore:
  lda player_score,x
  jsr debugAatX
playerScoreDex:
  dex
  bpl :-


  rts
.endproc
  
  


.proc debugAat2
  sta bcd_in
  jsr hex2Bcd
  lda bcd_out+1
  clc
  adc #$60
  sta bcdDisplay0+1
  lda bcd_out
  and #$F0
  clc
  ror
  ror
  ror
  ror
  clc
  adc #$60
  sta bcdDisplay1+1
  lda bcd_out
  and #$0F
  clc
  adc #$60
  sta bcdDisplay2+1
  lda bg_scroll_hi
  rts
.endproc
.proc debugAat1
  sta bcd_in
  jsr hex2Bcd
  lda bcd_out+1
  clc
  adc #$60
  sta bcdDisplay0
  lda bcd_out
  and #$F0
  clc
  ror
  ror
  ror
  ror
  clc
  adc #$60
  sta bcdDisplay1
  lda bcd_out
  and #$0F
  clc
  adc #$60
  sta bcdDisplay2
  lda bg_scroll_hi
  rts
.endproc

.proc debugAatX
  stx debug_temp
  sta bcd_in
  jsr hex2Bcd
  ldx debug_temp
  lda bcd_out+1
  clc
  adc #$60
  sta bcdDisplay0,x
  lda bcd_out
  and #$F0
  clc
  ror
  ror
  ror
  ror
  clc
  adc #$60
  sta bcdDisplay1,x
  lda bcd_out
  and #$0F
  clc
  adc #$60
  sta bcdDisplay2,x
  rts
.endproc

.proc hex2Bcd


        STA bcd_in      
        TAY             ; Save the input to restore later if desired.
        lda #0
        sta bcd_out+1   ; Begin by storing 0 in the output bytes.
        lda #0
        sta bcd_out     ; (NMOS 6502 will need LDA #0, STA ...)
        lda bcd_in
        LDX #8

 htd1: ASL bcd_in
        ROL bcd_out
        ROL bcd_out+1

        DEX             ; The shifting will happen seven times.  After
        BEQ htd3       ; the last shift, you don't check for digits of
                        ; 5 or more.
        LDA bcd_out
        AND #$0F
        CMP #05
        BMI htd2

        CLC
        LDA bcd_out
        ADC #3
        STA bcd_out

 htd2: LDA bcd_out
        CMP #$50
        BMI htd1

        CLC
        ADC #$30
        STA bcd_out

        jmp htd1       ; NMOS 6502 can use JMP.

 htd3: STY bcd_in      ; Restore the original input.
        RTS

.endproc
