.include "nes.inc"
.include "global.inc"

;PLAYER_JOIN_TIMER_AMT = 80
PLAYER_JOIN_TIMER_AMT = 30

.segment "RODATA"

PRESSA_START_1LO:
.byte $A4
.byte $B4
.byte $A4
.byte $B4
PRESSA_START_2LO:
.byte $E4
.byte $F4
.byte $E4
.byte $F4
PRESSA_START_HI:
.byte $24
.byte $24
.byte $26
.byte $26
PRESSSTART_HI2:
.byte $25
.byte $25
.byte $27
.byte $27
PRESSSTART_1LO:
.byte $82
.byte $92
.byte $82
.byte $92


.segment "BSS"

player_join_timer:    .res 1
ready_addr_lo:        .res 1
ready_addr_hi:        .res 1
new_palette_color:    .res 4
start_game:           .res 1

player_msg_draw:      .res 4

MSG_NONE    = 0
MSG_BLANK   = 1
MSG_START   = 2

.segment "CODE"


.proc startScreen
  ;set to no players in game, clear palette updater
  lda #0
  sta player_join_timer
  sta start_game
  ldx #3
:
  sta player_ingame,x
  sta player_ai,x
  sta new_palette_color,x
  dex
  bpl :-

  jsr setupStartScreen
startScreenLoop:

  ;keep looping until start press 4 seconds after someone joins
  lda player_join_timer
  beq :+
  dec player_join_timer
:

  ;update music

  jsr read_pads
  jsr startScreenCheckControls
  jsr waitForVBlank
  lda start_game
  bne startGame
  jsr renderStartScreen
  musicUpdate
  jmp startScreenLoop
startGame:
  jsr seeIfNeedsAi
  rts

.endproc
  

.proc seeIfNeedsAi

  ;count how many players in game
  ldx #3
  lda #0
  clc
:
    adc player_ingame,x
  dex
  bpl :-
  
  cmp #1
  bne done

  ;add ai's
  ldx #3
:
    lda player_ingame,x
    bne :+
      lda #1
      sta player_ai,x
      sta player_ingame,x
:
  dex
  bpl :--


done:
  rts

.endproc

.proc setupStartScreen
  jsr ppu_screen_off

  ldx #0
  stx oam_used
  jsr ppu_clear_oam

  clearMainScreen

  ;set palettes to dark gray background, white text
  lda #PAL_BASE
  sta PPUADDR
  lda #PAL_BG
  sta PPUADDR
  lda #COLOR_BLACK
  ldx #3
:
  ;advance counter
  ldy PPUDATA
  ldy PPUDATA
  sta PPUDATA
  ldy PPUDATA
  dex
  bpl :-

  ; draw four frames with "press start" on them
  drawFrameAt #0,#0,#16,#12,#0,#SCREEN_RIGHT
  drawFrameAt #16,#0,#16,#12,#1,#SCREEN_RIGHT
  drawFrameAt #0,#16,#16,#12,#2,#SCREEN_RIGHT
  drawFrameAt #16,#16,#16,#12,#3,#SCREEN_RIGHT
  

  ;drawString press_start,$24,$C2
  drawString pressatojoina,$24,$A4
  drawString pressatojoinb,$24,$E4

  drawString pressatojoina,$24,$B4
  drawString pressatojoinb,$24,$F4

  drawString pressatojoina,$26,$A4
  drawString pressatojoinb,$26,$E4

  drawString pressatojoina,$26,$B4
  drawString pressatojoinb,$26,$F4

  ;drawString press_start,$26,$D2
  ;drawString pressstartwhenalla,$24,$92
  ;drawString pressstartwhenallb,$24,$D2
  ;drawString pressstartwhenallc,$25,$12

  enableMainScreen

  lda #MSG_NONE
  sta player_msg_draw
  sta player_msg_draw+1
  sta player_msg_draw+2
  sta player_msg_draw+3

  rts
.endproc


.proc startScreenCheckControls

  ;iterate through each player, check controls
  ldx #3
:
    lda new_keys,x
    and #KEY_START
    beq noStartPress
      jsr startScreenStartPressed
noStartPress:
  dex
  bpl :-


  ldx #3
:
    lda new_keys,x
    and #KEY_A
    beq noAPress
      jsr startScreenAPressed
noAPress:
  dex
  bpl :-
  rts
.endproc

.proc startScreenStartPressed
  
  ; (if an already-joined player presses start, and not-join timer complete,
  ;  start the game)
  lda player_ingame,x
  bne :+
    ;otherwise, handle like A pressed
    jmp startScreenAPressed

:
  ;player already in game, maybe start?
  lda player_join_timer
  bne done
  ;start the game
  lda #1
  sta start_game
done:
  rts

.endproc


.proc startScreenAPressed

  ;player isn't already in game, add them:
  ;if start (or A button) pressed, add them to game, 
  lda #1
  sta player_ingame,x

  ;Reset not-join timer
  lda #PLAYER_JOIN_TIMER_AMT
  sta player_join_timer

  ;set indicator
  ; to change palette and change text for them.
  lda player_colors,x
  sta new_palette_color,x

  lda #MSG_BLANK
  sta player_msg_draw,x


done:
  rts

.endproc

.proc renderStartScreen

xTmp  = 0
loTmp = 1
hiTmp = 2

  ;blank out sprites
  lda #0
  sta OAMADDR
  lda #>OAM
  sta OAM_DMA

  ;check for palette changes and render them
  ldx #3
:
  ldy new_palette_color,x
  beq afterPlayerPalChange

  lda #PAL_BASE
  sta PPUADDR
  ;figure out pal address to write (player X 4 + 2)
  txa
  asl
  asl
  clc
  adc #2
  sta PPUADDR
  sty PPUDATA

  lda #PAL_BASE
  sta PPUADDR
  ;figure out pal address to write (player X 4 + 2)
  txa
  asl
  asl
  clc
  adc #PAL_OBJ + 2
  sta PPUADDR
  sty PPUDATA


afterPlayerPalChange:
  dex
  bpl :-


  ;check for text changes and render them
  ldx #NUM_PLAYERS-1
:
    lda player_msg_draw,x
    beq bottomTrampoline
      cmp #MSG_BLANK
      beq blankOutPlayer
        
drawStartMessage:

    stx xTmp
    lda #<pressstartwhenalla
    sta misc_addr_ptr
    lda #>pressstartwhenalla
    sta misc_addr_ptr+1
    ldy xTmp
    lda PRESSA_START_HI,y
    sta hiTmp
    tax
    lda PRESSSTART_1LO,y
    sta loTmp
    jsr drawText
    lda #<pressstartwhenallb
    sta misc_addr_ptr
    lda #>pressstartwhenallb
    sta misc_addr_ptr+1
    ldx hiTmp
    lda loTmp
    clc
    adc #$40
    sta loTmp
    jsr drawText
    lda #<pressstartwhenallc
    sta misc_addr_ptr
    lda #>pressstartwhenallc
    sta misc_addr_ptr+1
    ldx hiTmp
    inx
    lda loTmp
    sec
    sbc #$C0
    jsr drawText

    ldx xTmp
    lda #0
    sta player_msg_draw,x
bottomTrampoline:
    jmp loopBottom


blankOutPlayer:
        lda PRESSA_START_HI,x
        sta PPUADDR
        lda PRESSA_START_1LO,x
        sta PPUADDR
        lda #2
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        lda PRESSA_START_HI,x
        sta PPUADDR
        lda PRESSA_START_2LO,x
        sta PPUADDR
        lda #2
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        sta PPUDATA
        lda #MSG_START
        sta player_msg_draw,x
loopBottom:
  dex
  bmi :+
  jmp :-

:


fixScrollAtEnd:
  lda #0
  sta PPUSCROLL
  sta PPUSCROLL

  
  rts
.endproc
