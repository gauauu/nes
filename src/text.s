.include "nes.inc"
.include "global.inc"


;
;press start
;normal round
;battle
;speed
;danger
;survival
;narrow

;characters needed:
;eprstaoundmlbgviwy


.segment "CODE"
;addr of text in misc_addr_ptr
;A is lo ppu addr
;X is hi ppu addr
.proc drawText

  ;first update draw position
  stx PPUADDR
  sta PPUADDR

  ;start drawing
  ldy #0
  lda (misc_addr_ptr),y
  tay
:
  lda (misc_addr_ptr),y
  sta PPUDATA
  dey
  bne :-
  rts

.endproc

;addr of text line in misc_addr_ptr
;A is lo ppu addr
;X is hi ppu addr
.proc drawTextLines
tmplo = 0
tmphi = 1

  sta tmplo
  stx tmphi

  stx PPUADDR
  sta PPUADDR

  ldy #0
  lda (misc_addr_ptr),y
  tay
loop:

    lda (misc_addr_ptr),y
    cmp #LINE_BREAK
    bne noLineBreak
      ;if line break,
      ;increment line
      lda tmplo
      clc
      adc #32
      sta tmplo
      tax
      lda tmphi
      adc #0
      sta tmphi
      ;and write it to PPUADDR
      sta PPUADDR
      stx PPUADDR
      jmp loopBottom

noLineBreak:
    sta PPUDATA
loopBottom:
  dey
  bne loop
  rts

.endproc


;addr of text line in misc_addr_ptr
;A is lo ppu addr
;X is hi ppu addr
.proc drawTextCentered
tmplo = 0
tmphi = 1

  ;store a and x for later
  sta tmplo
  stx tmphi

  ;first see how long it is and
  ;center it
  lda #32               ;screen width
  sec
  sbc (misc_addr_ptr),y 
  ;now a is amount of blank space
  ;total. divide by 2 for left side
  clc
  ror
  
  ;add it to the text address
  add16 tmplo,tmphi

  ;first update draw position
  lda tmphi
  sta PPUADDR
  lda tmplo
  sta PPUADDR

  ;start drawing
  ldy #0
  lda (misc_addr_ptr),y
  tay
:
  lda (misc_addr_ptr),y
  sta PPUDATA
  dey
  bne :-
  rts
.endproc
