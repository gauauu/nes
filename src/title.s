.include "nes.inc"
.include "global.inc"

.segment "BSS"

NUM_STARS = 28
CREDIT_SECONDS=2

starX:          .res NUM_STARS
starY:          .res NUM_STARS
untilNextStar:  .res 1
creditTimer:    .res 1
currentCredit:  .res 1

creditHi:       .res 1
creditLo:       .res 1
creditStatus:   .res 1

CREDIT_STATUS_SKIP  = 0
CREDIT_STATUS_CLEAR = 1
CREDIT_STATUS_WRITE = 2

.segment "RODATA"

creditMapLo:
  .byte <credit0
  .byte <credit1
  .byte <credit2
  .byte <credit3
  .byte <credit4
creditMapHi:
  .byte >credit0
  .byte >credit1
  .byte >credit2
  .byte >credit3
  .byte >credit4

.segment "CODE"

.proc titleScreen
  jsr setupTitle
titleLoop:

  ;keep looping until it presses start
  jsr read_pads
  ldx #3
:
  lda cur_keys,x
  and #KEY_START
  beq notStart
  rts
notStart:
  dex
  bpl :-

  inc timer
  jsr updateTitleSprites

  jsr preRenderTitle
  jsr preRenderTitleSprites
  jsr waitForVBlank
  jsr renderTitle
  musicUpdate
  jmp titleLoop

.endproc


.proc updateTitleSprites 
tmpx = 0

  lda untilNextStar
  beq :+
    dec untilNextStar
:


  ldx #NUM_STARS-1
:
  lda starY,x
  beq unused
    inc starY,x
    jmp loopEnd
  
unused:
  ;maybe add this one?
    lda untilNextStar
    bne loopEnd
    ;spawn star
      stx tmpx
      jsr randomNumber
      ldx tmpx
      sta starX,x
      lda #1
      sta starY,x
      jsr randomNumber
      and #%00001111
      sta untilNextStar
      ldx tmpx
loopEnd:
  dex
  bpl :-
  rts

.endproc

.proc preRenderTitleSprites
  lda #0
  sta oam_used
  ldy #NUM_STARS-1
:
    lda starY,y
    beq next
      ;render the star
      writeSprite {starX,y},{starY,y},#$33,#%00100000
next:
  dey
  bpl :-
  ldx oam_used
  jsr ppu_clear_oam
  rts

.endproc

;A holds addr high
;X holds addr low
;Y holds graphic start
.proc drawTitleLineImpl
  sta PPUADDR
  stx PPUADDR
  ldx #15
:
  sty PPUDATA
  iny
  dex
  bpl :-
  rts
.endproc

.proc drawTitleHalfLineImpl
  sta PPUADDR
  stx PPUADDR
  ldx #7
:
  sty PPUDATA
  iny
  dex
  bpl :-
  rts
.endproc

.macro drawTitleLine hi,lo,tile
  lda #hi
  ldx #lo
  ldy #tile
  jsr drawTitleLineImpl
.endmacro

.macro drawTitleHalfLine hi,lo,tile
  lda #hi
  ldx #lo
  ldy #tile
  jsr drawTitleHalfLineImpl
.endmacro


.proc setupTitle

  lda #0
  sta untilNextStar
   

  lda #CREDIT_SECONDS
  sta creditTimer
  lda #0
  sta currentCredit
  lda #CREDIT_STATUS_SKIP
  sta creditStatus

  ;clear background
  ldx #$24
  lda #$00
  ldy #$AA
  jsr ppu_clear_nt
  ;play some music?

  drawTitleLine $25,$05,$B0
  drawTitleLine $25,$25,$C0
  drawTitleLine $25,$45,$D0
  drawTitleLine $25,$65,$E0

  drawTitleHalfLine $25,$8A,$88
  drawTitleHalfLine $25,$92,$B7
  sty PPUDATA
  drawTitleHalfLine $25,$AA,$98
  drawTitleHalfLine $25,$B2,$C7
  sty PPUDATA
  drawTitleHalfLine $25,$CA,$A8
  drawTitleHalfLine $25,$D2,$D7
  sty PPUDATA
  
  lda #$25
  sta PPUADDR
  lda #$F8
  sta PPUADDR
  ldx #$ED
  stx PPUDATA
  inx
  stx PPUDATA
  inx
  stx PPUDATA

  ;set the color to black for text
  lda #PAL_BASE
  sta PPUADDR
  lda #$00
  sta PPUADDR
  lda #COLOR_BLACK
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA


  ;change palette for press start text
  lda #$27
  sta PPUADDR
  lda #$E0
  sta PPUADDR
  ldx #24
:
  lda #%01010101
  sta PPUDATA
  dex
  bpl :-


  ;drawString players,$26,$8
  drawString bestwith,$26,$06
  drawString press_start,$26,$4B

  lda #<credit0
  sta misc_addr_ptr
  lda #>credit0
  sta misc_addr_ptr+1
  ldx #$26
  lda #$C0
  jsr drawTextLines

  ; Turn the screen on
  ldx #0
  ldy #0
  lda #VBLANK_NMI|BG_0000|OBJ_1000|NT_2400
  sec
  jsr ppu_screen_on

  rts
.endproc

.proc preRenderTitle


  lda timer
  bne done

  ;dec creditTimer
  ;bne :+

  ;lda #CREDIT_SECONDS
  ;sta creditTimer

  ;setup renderer to display
  ldx currentCredit
  lda creditMapLo,x
  sta creditLo
  lda creditMapHi,x
  sta creditHi

  ;reset to start
  inc currentCredit
  lda currentCredit
  cmp #5
  bne :+
  lda #0
  sta currentCredit
:

  lda #CREDIT_STATUS_CLEAR
  sta creditStatus


done:
  rts
.endproc

.proc renderTitle
  ; Copy the display list from main RAM to the PPU
  lda #0
  sta OAMADDR
  lda #>OAM
  sta OAM_DMA

  lda creditStatus
  beq done

  cmp #CREDIT_STATUS_CLEAR
  beq clearTexts
    
drawText:
    lda creditLo
    sta misc_addr_ptr
    lda creditHi
    sta misc_addr_ptr+1
    ldx #$26
    lda #$C0
    jsr drawTextLines
    lda #0
    sta creditStatus
    jmp resetScroll

clearTexts:
    lda #$26
    sta PPUADDR
    lda #$C0
    sta PPUADDR
    lda #0
    ldx #96
:
    sta PPUDATA
    dex
    bpl :-
    inc creditStatus

resetScroll:
done:

  ;debug
    ;lda #$26
    ;sta PPUADDR
    ;lda #$00
    ;sta PPUADDR
    ;lda creditStatus
    ;clc
    ;adc #$60
    ;sta PPUDATA
    ;;;;

    lda #0
    sta PPUSCROLL
    sta PPUSCROLL


  rts
.endproc
