.include "nes.inc"
.include "global.inc"

.segment "BSS"
ws_timer: .res 1
ws_dx:    .res 1

.segment "CODE"

.proc showWinnerScreen
  jsr setupWinnerScreen

  ;pre-run the title sprites
  ldx #30
  sta ws_timer
:
  jsr updateTitleSprites
  dec ws_timer
  bpl :-

  musicStop

  lda #6
  sta ws_timer
:
    dec ws_timer
    beq loopEnd

    ldx #60
    stx timer
:
      jsr updateWinnerSprite
      jsr updateTitleSprites
      jsr preRenderWinnerScreen
      jsr waitForVBlank
      jsr renderWinnerScreen
      musicUpdate

    dec timer
    bne :-
  jmp :--

loopEnd:
  jmp reset_handler
.endproc


.proc updateWinnerSprite
  ldx phase_winner
  lda ws_dx
  bmi moveLeft
moveright:
    add16 {player_xlo,x},{player_xhi,x}
    lda player_xhi,x
    cmp #220
    bcc done
      lda #$FF
      sta ws_dx
    jmp done
moveLeft:
    sub16 {player_xlo,x},{player_xhi,x}
done:
  rts

.endproc

.proc setupWinnerScreen
  jsr ppu_screen_off

  ldx #0
  stx oam_used
  jsr ppu_clear_oam

  ldx #$20
  lda #$00
  ldy #00
  jsr ppu_clear_nt

  ;setup the player sprites
  ldx phase_winner
  lda #PLAYER_STATUS_NORMAL
  sta player_status,x
  lda #50
  sta player_xhi,x
  lda #180
  sta player_yhi,x
  lda #127
  sta ws_dx
  

  drawFrameAt #4,#4,#24,#8,phase_winner,#SCREEN_LEFT
  drawString winner,$21,$08

  enableLeftScreen

  rts
.endproc

.proc preRenderWinnerScreen
  jsr preRenderTitleSprites
  ldx phase_winner
  jsr preRenderPlayerSprite
  rts
.endproc

.proc renderWinnerScreen
  lda #0

  sta OAMADDR
  lda #>OAM
  sta OAM_DMA

  rts
.endproc


