#!/usr/bin/python


def main():
    import sys
    import re
    argv = sys.argv
    if len(argv) < 3:
        print "Must supply source and destination files"
        sys.exit(1)
    outfile = open(argv[2], "w")
    infile = open(argv[1], "r")
    outfile.write(".include \"nes.inc\"\n.include \"global.inc\"\n\n")
    outfile.write("\n.segment \"CODE\"\n\n")
    lines = infile.readlines()
    for line in lines:
        line = line.strip()
        parts = line.split(":")
        subject = parts[0]
        label = parts[0].replace(" ", "_");
        if len(parts) > 1:
            subject = parts[1].lower()
        chars = len(subject)
        outfile.write(label + ":\n")
        outfile.write(".byte " + str(chars) + "\n")
        for c in subject[::-1]:
            charNum = ord(c) - 32
            numberMatcher = re.compile("[0-9]")
            if (c == " "):
                outfile.write(".byte 2 ;space\n")
            elif (c == "|"):
                outfile.write(".byte 255; pipe - break\n")
            elif (c == "!"):
                outfile.write(".byte 106 ;!\n")
            elif (c == ","):
                outfile.write(".byte 91 ;,\n")
            elif (c == "/"):
                outfile.write(".byte 92 ;/\n")
            elif (c == "."):
                outfile.write(".byte 93 ;.\n")
            elif (c == ";"):
                outfile.write(".byte 94 ; ; but meaning :\n")
            elif (c == "-"):
                outfile.write(".byte 95 ; -\n")
            elif (c == "?"):
                outfile.write(".byte 107 ;?\n")
            elif (numberMatcher.match(c)):
                charNum = 96 + int(c)
                outfile.write(".byte " + str(charNum) + ";" + c + "\n")
            else: 
                outfile.write(".byte " + str(charNum) + ";" + c + "\n")
        outfile.write(";.byte \"" + subject + "\"\n\n")


if __name__=='__main__':
    main()
